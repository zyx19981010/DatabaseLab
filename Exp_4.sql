/*
INSERT基本语句(插入全部列的数据)
插入一条顾客记录，要求每列都给一个合理的值。
*/
INSERT INTO Customer
VALUES(30, '张三', '北京市', 40, '010-51001199', 0.00, 'Northeast', 'VIP Customer');

/*
INSERT基本语句(插入部分列的数据)
插入一条订单记录，给出必要的几个字段值
*/
INSERT INTO Lineitem(orderkey, Linenumber, partkey, suppkey, quantity, shipdate)
VALUES(862, ROUND(RAND() * 100, 0), 479, 1, 10, '2012-3-6');

/*
批量数据INSERT语句
*/
-- 1.创建一个新的顾客表，把所有中国籍顾客插人到新的顾客表中。--
CREATE TABLE NewCustomer like Customer;
INSERT INTO NewCustomer
SELECT C.*
FROM Customer C, Nation N
WHERE C.nationkey = N.nationkey AND N.name = '中国';

-- 2.创建一个顾客购物统计表，记录每个顾客及其购物总数和总价等信息。 --
CREATE TABLE ShoppingStat
(
    custkey INTEGER,
    quantity REAL,
    totalprice REAL
);
INSERT INTO ShoppingStat
SELECT C.custkey, Sum(L.quantity), Sum(O.totalprice)
FROM Customer C, Orders O, Lineitem L
GROUP BY C.custkey;

-- 3.倍增零件表的数据，多次重复执行，直到总记录数达到50万为止。--
INSERT INTO Part
SELECT partkey + (SELECT COUNT(*) FROM Part),
    name, mfgr, brand, type, size, container, retailprice, comment
FROM Part;

/*
UPDATE语句(修改部分记录的部分列值)
"金仓集团”供应的所有零件的供应成本价下降10%。
*/
UPDATE PartSupp
SET supplycost = supplycost * 0.9
WHERE suppkey = (SELECT suppkey
FROM Supplier
WHERE name = '金仓集团');

/*
UPDATE语句(利用一个表中的数据修改另外一个表中的数据)
利用Part表中的零售价格来修改Lineitem中的extendedprice，其中extendedprice = Part.retailprice * quantity。
*/
UPDATE Lineitem L, Part P
SET L.extendprice = P.retailprice * L.quantity
WHERE L.partkey = P.partkey;

/*
DELETE基本语句(删除给定条件的所有记录)
删除顾客张三的所有订单记录。
*/
DELETE FROM Lineitem
WHERE orderkey IN (SELECT orderkey
FROM Orders O, Customer C
WHERE O.custkey = C.custkey AND C.name = '张三');
DELETE FROM Orders
WHERE custkey = (
    SELECT custkey
    FROM Customer
    WHERE name = '张三'
);


