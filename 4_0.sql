/* After触发器 */
# UPDATE触发器
CREATE TRIGGER TRI_Lineitem_Price_UPDATE
  AFTER UPDATE
  ON Sales.Lineitem
  FOR EACH ROW BEGIN
  DECLARE new_total REAL;
  IF (OLD.extendprice != NEW.extendprice)
  OR (OLD.discount != NEW.discount)
  OR (OLD.tax != NEW.tax) THEN
    SET new_total = NEW.extendprice * (1 - NEW.discount) * (1 + NEW.tax);
    UPDATE Orders SET totalprice = new_total;
  end if;
END;

# Insert触发器
CREATE TRIGGER TRI_Lineitem_Price_INSERT
  AFTER INSERT
  ON Sales.Lineitem
  FOR EACH ROW BEGIN
  DECLARE L_valuediff REAL;
  SET L_valuediff = NEW.extendprice * (1 - NEW.discount) * (1 + NEW.tax);
  UPDATE Orders SET totalprice = totalprice + L_valuediff
  WHERE orderkey = NEW.orderkey;
end;

# Delete触发器
CREATE TRIGGER TRI_Lineitem_Price_DELETE
  AFTER DELETE
  ON Sales.Lineitem
  FOR EACH ROW
  BEGIN
    DECLARE L_valuediff REAL;
    SET L_valuediff = - OLD.extendprice * (1 - OLD.discount) * (1 + OLD.tax);
    UPDATE Orders SET totalprice = totalprice + L_valuediff
    WHERE orderkey = OLD.orderkey;
  end;

# 测试触发器TRI_Lineitem_Price_UPDATE
# 查看1854号订单的含税折扣总价
SELECT totalprice FROM Orders WHERE orderkey = 1;
# 激活触发器
UPDATE Lineitem SET tax = tax + 0.005 WHERE orderkey = 1 AND linenumber = 1;
# 再次查看1854号订单的含税折扣总价
SELECT totalprice FROM Orders WHERE orderkey = 1;

/*Before触发器*/
# UPDATE触发器
CREATE TRIGGER TRI_Lineitem_Quantity_UPDATE
  BEFORE UPDATE
  ON Sales.Lineitem
  FOR EACH ROW BEGIN
  DECLARE L_valuediff INTEGER;
  DECLARE L_availqty INTEGER;
  DECLARE message varchar(200);
  SET L_valuediff = NEW.quantity - OLD.quantity;
  SELECT availqty INTO L_availqty
  FROM PartSupp
  WHERE partkey = NEW.partkey AND suppkey = NEW.suppkey;
  IF (L_availqty - L_valuediff >= 0) THEN
    UPDATE PartSupp SET availqty = availqty - L_valuediff
    WHERE partkey = NEW.partkey AND suppkey = NEW.suppkey;
  ELSE
    SET message = 'NOT ENOUGH available quantity';
    SIGNAL SQLSTATE 'HY000' SET MESSAGE_TEXT = message;
  end if;
end;

# INSERT触发器
CREATE TRIGGER TRI_Lineitem_Quantity_INSERT
  BEFORE INSERT
  ON Sales.Lineitem
  FOR EACH ROW BEGIN
  DECLARE L_valuediff INTEGER;
  DECLARE L_availqty INTEGER;
  DECLARE message varchar(200);
  SET L_valuediff = NEW.quantity;
  SELECT availqty INTO L_availqty
  FROM PartSupp
  WHERE partkey = NEW.partkey AND suppkey = NEW.suppkey;
  IF (L_availqty - L_valuediff >= 0) THEN
    UPDATE PartSupp SET availqty = availqty - L_valuediff
    WHERE partkey = NEW.partkey AND suppkey = NEW.suppkey;
  ELSE
    SET message = 'NOT ENOUGH available quantity';
    SIGNAL SQLSTATE 'HY000' SET MESSAGE_TEXT = message;
  end if;
end;

# DELETE触发器
CREATE TRIGGER TRI_Lineitem_Quantity_DELETE
  BEFORE DELETE
  ON Lineitem
  FOR EACH ROW BEGIN
  DECLARE L_valudediff INTEGER;
  DECLARE L_availqty INTEGER;
  SET L_valudediff = -OLD.quantity;
  UPDATE PartSupp
      SET availqty = availqty - L_valudediff
  WHERE partkey = OLD.partkey AND suppkey = OLD.suppkey;
end;

# 验证触发器TRI_Lineitem_Quantity_UPDATE
# 执行查询
SELECT L.partkey, L.suppkey, L.quantity, PS.availqty
FROM Lineitem L, PartSupp PS
WHERE L.partkey = PS.partkey AND L.suppkey = PS.suppkey
AND L.orderkey = 1 AND linenumber = 1;
# 激活触发器
UPDATE Lineitem SET quantity = quantity + 5
WHERE orderkey = 1 AND linenumber = 1;
# 再次执行查询
SELECT L.partkey, L.suppkey, L.quantity, PS.availqty
FROM Lineitem L, PartSupp PS
WHERE L.partkey = PS.partkey AND L.suppkey = PS.suppkey
AND L.orderkey = 1 AND linenumber = 1;

