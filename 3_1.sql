use Sales;
-- 创建表时定义实体完整性（列级实体完整性） --
CREATE TABLE Supplier_1(
  suppkey INTEGER PRIMARY KEY,
  name CHAR(25),
  address VARCHAR(40),
  nationkey INTEGER,
  phone CHAR(15),
  acctbal REAL,
  comment VARCHAR(101)
);

-- 创建表时定义实体完整性（表级实体完整性） --
CREATE TABLE Supplier_2(
  suppkey INTEGER,
  name CHAR(25),
  address VARCHAR(40),
  nationkey INTEGER,
  phone CHAR(15),
  acctbal REAL,
  comment VARCHAR(101),
  CONSTRAINT PK_supplier PRIMARY KEY (suppkey)
);

-- 创建表后定义实体完整性 --
CREATE TABLE Supplier_3(
  suppkey INTEGER,
  name CHAR(25),
  address VARCHAR(40),
  nationkey INTEGER,
  phone CHAR(15),
  acctbal REAL,
  comment VARCHAR(101)
);
ALTER TABLE Supplier_3 ADD CONSTRAINT PK_supplier PRIMARY KEY (suppkey);

-- 定义实体完整性（主码由多个属性组成） --
CREATE TABLE PartSupp_1(
  partkey INTEGER,
  suppkey INTEGER,
  availqty INTEGER,
  supplycost REAL,
  comment VARCHAR(199),
  PRIMARY KEY (partkey, suppkey)
);

-- 有多个候选码时定义实体完整性 --
CREATE TABLE nation(
  nationkey INTEGER PRIMARY KEY ,
  name CHAR(25) UNIQUE ,
  regionkey INTEGER,
  comment VARCHAR(152)
);

-- 删除实体完整性 --
ALTER TABLE nation DROP PRIMARY KEY;

-- 验证实体完整性 --
INSERT INTO Supplier_3 (suppkey, name, address, nationkey, phone, acctbal, comment) 
VALUES (11, 'test1', 'test1', 101, '12345678', 0.0, 'test1');

INSERT INTO Supplier_3 (suppkey, name, address, nationkey, phone, acctbal, comment) 
VALUES (11, 'test2', 'test2', 102, '23456789', 0.0, 'test2');

DROP TABLE Supplier_1, Supplier_2, Supplier_3, PartSupp_1, nation;


