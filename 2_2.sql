-- 显示当前审计开关状态 --
show status like '%audit%L';

-- 打开审计开关 --
set global server_audit_logging=on;

-- 数据库操作审计 --
-- 删除审计 --
set global server_audit_events='QUERY_DML_NO_SELECT';

-- 执行操作 --
DELETE FROM Sales.Customer WHERE custkey = 1011;

-- 查看审计信息 --


-- 设置Alter审计 --
set global server_audit_events='QUERY_DDL';

-- 查看所有语句级审计 --
show variables like 'server_audit_events';

-- 执行Alter操作 --
ALTER TABLE Customer ADD column u INT;

-- 查看审计信息 --