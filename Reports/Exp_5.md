# 实验 1.5 视图实验

## 实验目的

熟悉SQL语言有关视图的操作，能够熟练使用SQL语句来创建需要的视图，定义数据库外模式，并能使用所创建的视图实现数据管理。

## 实验内容和要求

针对给定的数据库模式，以及相应的应用需求，创建视图和带WITH CHECK OPTION的 
视图，并验证视图WITH CHECK OPTION选项的有效性、理解和掌握视图消解执行原理，掌 
握可更新视图和不可更新视图的区别。

## 实验重点和难点

实验重点：创建视图。
实验难点：可更新的视图和不可更新的视图之区别，WITH CHECK OPTION的验证。

## 实验SQL语句

```sql
/*
创建视图(省略视图列名)
创建一个"海大汽配”供应商供应的零件视图V_DLMU_PartSuppl ,要求列出供应零件 的编号、零件名称、可用数量、零售价格、供应价格和备注等信息。
*/
CREATE VIEW V_DLMU_PartSupp1 AS
SELECT P.partkey, P.name, PS.availqty, P.retailprice, PS.supplycost, P.comment
FROM Part P, PartSupp PS, Supplier S
WHERE P.partkey = PS.partkey
AND S.suppkey = PS.suppkey
AND S.name = '海大汽配';

/*
创建视图(不能省略列名的情况)
创建一个视图V_CustAvgOrder，按顾客统计平均每个订单的购买金额和零件数量，要求输出顾客编号，姓名，平均购买金额和平均购买零件数量。
*/
CREATE VIEW V_CustAvgOrder(custkey, cname, avgprice, avgquantity) AS
SELECT C.custkey, MAX(C.name), AVG(O.totalprice), AVG(L.quantity)
FROM Customer C, Orders O, Lineitem L
WHERE C.custkey = O.custkey AND L.orderkey = O.orderkey
GROUP BY C.custkey;

/*
创建视图(WITH CHECK OPTION)
使用WITH CHECK OPTION,创建一个“海大汽配”供应商供应的零件视图V_DLMU_ PartSupp2,
要求列出供应零件的编号、可用数量和供应价格等信息。
然后通过该视图分别增加、刪除和修改一条"海大汽配”零件供应记录，验证 WITH CHECK OPTION是否起作用。
*/
CREATE VIEW V_DLMU_PartSupp2
AS
SELECT partkey, suppkey, availqty, supplycost
FROM PartSupp
WHERE suppkey = (
    SELECT suppkey
    FROM Supplier
    WHERE name = '海大汽配'
)
WITH CHECK OPTION;
-- 可以执行 --
INSERT INTO V_DLMU_PartSupp2
VALUES(58889, 5048, 704, 77760);
-- 可以执行 --
UPDATE V_DLMU_PartSupp2
SET supplycost = 12
WHERE supplycost = 58889;
-- 可以执行 --
DELETE FROM V_DLMU_PartSupp2
WHERE suppkey = 58889;

/*
可更新的视图(行列子集视图)
创建一个“海大汽配”供应商供应的零件视图V_DLMU_PartSupp4,要求列出供应零件的编号、可用数量和供应价格等信息。
然后通过该视图分别增加、删除和修改一条“海大汽配”零件供应记录，
验证该视图是否是可更新的，并比较上述"(3) 创建视图"实验任务 
与本任务结果有何异同。
*/
CREATE VIEW V_DLMU_PartSupp3
AS
SELECT partkey, suppkey, availqty, supplycost
FROM PartSupp
WHERE suppkey = (
    SELECT suppkey
    FROM Supplier
    WHERE name = '海大汽配'
);
-- 可以执行 --
INSERT INTO V_DLMU_PartSupp2
VALUES(58889, 5048, 704, 77760);
-- 可以执行 --
UPDATE V_DLMU_PartSupp2
SET supplycost = 12
WHERE supplycost = 58889;
-- 可以执行 --
DELETE FROM V_DLMU_PartSupp2
WHERE suppkey = 58889;

/*
不可更新的视图
以下列语句验证（2）中创建的视图是否可更新
*/

-- 执行失败，不允许直接对含聚合函数的视图作修改操作 --
INSERT INTO V_CustAvgOrder
VALUES(100000, NULL, 20, 2000);

/*
删除视图(RESTRICT/CASCADE)
创建顾客订购零件明细视图V_CUStOrd，要求列出顾客编号、姓名、购买零件数、金额， 
然后在该视图的基础上，再创建(2)的视图V_CustAvgOrder，然后使用RESTRICT选项删除 
视图V_CustOrd，观察现象并解释原因。利用CASCADE选项删除视图V_CustOrd，观察现 
象并检查V_CustAvgOrder是否存在，解释原因？
*/
CREATE VIEW V_CustOrd(custkey, cname, qty, extprice)
AS
SELECT C.custkey = O.custkey
AND O.orderkey = L.orderkey;

CREATE VIEW V_CustAvgOrder(custkey, cname, avgqty, avgprice)
AS
SELECT custkey, MAX(cname), AVG(qty), AVG(extprice)
FROM V_CustOrd
GROUP BY custkey;

-- 执行失败，由于存在另一个视图与之相关 --
DROP VIEW V_CustOrd RESTRICT;
-- 执行成功且V_CustAvgOrder消失。由于CASCADE移除所有相关的视图。--
DROP VIEW V_CustOrd CASCADE;
```

## 实验过程与实验结果

```mariadb
/*
创建视图(省略视图列名)
创建一个"海大汽配”供应商供应的零件视图V_DLMU_PartSuppl ,要求列出供应零件 的编号、零件名称、可用数量、零售价格、供应价格和备注等信息。
*/
CREATE VIEW V_DLMU_PartSupp1 AS
SELECT P.partkey, P.name, PS.availqty, P.retailprice, PS.supplycost, P.comment
FROM Part P, PartSupp PS, Supplier S
WHERE P.partkey = PS.partkey
AND S.suppkey = PS.suppkey
AND S.name = '海大汽配';
```

![1539249085493](/home/williamzheng/.config/Typora/typora-user-images/1539249085493.png)



```mariadb
/*
创建视图(不能省略列名的情况)
创建一个视图V_CustAvgOrder，按顾客统计平均每个订单的购买金额和零件数量，要求输出顾客编号，姓名，平均购买金额和平均购买零件数量。
*/
CREATE VIEW V_CustAvgOrder(custkey, cname, avgprice, avgquantity) AS
SELECT C.custkey, MAX(C.name), AVG(O.totalprice), AVG(L.quantity)
FROM Customer C, Orders O, Lineitem L
WHERE C.custkey = O.custkey AND L.orderkey = O.orderkey
GROUP BY C.custkey;
```

![1539249254769](/home/williamzheng/.config/Typora/typora-user-images/1539249254769.png)



```mariadb
/*
创建视图(WITH CHECK OPTION)
使用WITH CHECK OPTION,创建一个“海大汽配”供应商供应的零件视图V_DLMU_ PartSupp2,
要求列出供应零件的编号、可用数量和供应价格等信息。
然后通过该视图分别增加、刪除和修改一条"海大汽配”零件供应记录，验证 WITH CHECK OPTION是否起作用。
*/
CREATE VIEW V_DLMU_PartSupp2
AS
SELECT partkey, suppkey, availqty, supplycost
FROM PartSupp
WHERE suppkey = (
    SELECT suppkey
    FROM Supplier
    WHERE name = '海大汽配'
)
WITH CHECK OPTION;
```

![1539249299485](/home/williamzheng/.config/Typora/typora-user-images/1539249299485.png)



```mariadb
INSERT INTO V_DLMU_PartSupp2
VALUES(58889, 5048, 704, 77760);
```

![1539249334852](/home/williamzheng/.config/Typora/typora-user-images/1539249334852.png)



```mariadb
UPDATE V_DLMU_PartSupp2
SET supplycost = 12
WHERE supplycost = 58889;
```

![1539249375737](/home/williamzheng/.config/Typora/typora-user-images/1539249375737.png)



```mariadb
DELETE FROM V_DLMU_PartSupp2
WHERE suppkey = 58889;
```

![1539249497484](/home/williamzheng/.config/Typora/typora-user-images/1539249497484.png)



```mariadb
/*
可更新的视图
(行列子集视图)
创建一个“海大汽配”供应商供应的零件视图V_DLMU_PartSupp4,要求列出供应零件的编号、可用数量和供应价格等信息。
然后通过该视图分别增加、删除和修改一条“海大汽配”零件供应记录，
验证该视图是否是可更新的，并比较上述"(3) 创建视图"实验任务 
与本任务结果有何异同。
*/
CREATE VIEW V_DLMU_PartSupp3
AS
SELECT partkey, suppkey, availqty, supplycost
FROM PartSupp
WHERE suppkey = (
    SELECT suppkey
    FROM Supplier
    WHERE name = '海大汽配'
);
```

![1539249568361](/home/williamzheng/.config/Typora/typora-user-images/1539249568361.png)



```mariadb
INSERT INTO V_DLMU_PartSupp3
VALUES(58889, 5048, 704, 77760);
```

![1539249725556](/home/williamzheng/.config/Typora/typora-user-images/1539249725556.png)



```mariadb
UPDATE V_DLMU_PartSupp3
SET supplycost = 12
WHERE supplycost = 58889;
```

![1539249790890](/home/williamzheng/.config/Typora/typora-user-images/1539249790890.png)



```mariadb
DELETE FROM V_DLMU_PartSupp3
WHERE suppkey = 58889;
```

![1539249931745](/home/williamzheng/.config/Typora/typora-user-images/1539249931745.png)



```mariadb
/*
不可更新的视图
以下列语句验证（2）中创建的视图是否可更新
*/
INSERT INTO V_CustAvgOrder
VALUES(100000, NULL, 20, 2000);
```

![1539250063008](/home/williamzheng/.config/Typora/typora-user-images/1539250063008.png)



```mariadb
/*
删除视图(RESTRICT/CASCADE)
创建顾客订购零件明细视图V_CUStOrd，要求列出顾客编号、姓名、购买零件数、金额， 
然后在该视图的基础上，再创建(2)的视图V_CustAvgOrder，然后使用RESTRICT选项删除
视图V_CustOrd，观察现象并解释原因。利用CASCADE选项删除视图V_CustOrd，观察现 
象并检查V_CustAVgOrder是否存在，解释原因？
*/
CREATE VIEW V_CustOrd(custkey, cname, qty, extprice)
AS
SELECT C.custkey, C.name, L.quantity, L.extendprice
from Customer C, Orders O, Lineitem L
WHERE C.custkey = O.custkey
AND O.orderkey = L.orderkey;
```

![1539251636350](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539251636350.png)



```mariadb
CREATE VIEW V_CustAvgOrder(custkey, cname, avgqty, avgprice)
AS
SELECT custkey, MAX(cname), AVG(qty), AVG(extprice)
FROM V_CustOrd
GROUP BY custkey;
```

![1539251683540](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539251683540.png)



```mariadb
DROP VIEW V_CustOrd RESTRICT;
```



![1539251738924](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539251738924.png)



```mariadb
DROP VIEW V_CustOrd CASCADE;
```

![1539251822981](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539251822981.png)

## 实验总结

本次实验进行了视图相关的各种操作。视图是对于实体表的一种映射，因此，我们在创建与操作视图时，一定要谨记我们的操作实际上是在实体的表上完成的。这种认识对于理解视图的增删改查与完整性约束都有帮助。

## 思考题

1. 请分析视图和基本表在使用方面有哪些异同，并设计相应的例子加以验证。

   答：视图中的数据来源于基本表，两者都可以在权限范围内进行基本的数据查询操作。但在实现上，实际的数据存放于基本表中，视图中只存在数据的定义。在修改数据时，可以对基本表中的数据进行任意的修改，而对视图中数据的修改是受限的。

2. 请具体分析修改基本表的结构对相应的视图会产生何种影响？

   答：如果被修改基本表的结构变动不涉及到视图中的属性，那么对视图没有影响。如果有视图中的属性在基本表中被移除了，那么该属性也会从视图中被移除。