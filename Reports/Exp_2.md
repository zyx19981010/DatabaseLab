# 实验 1.2 数据基本查询实验

## 实验目的

掌握SQL程序设计基本规范，熟练运用SQL语言实现数据基本査询，包括单表査询、分 
组统计査询和连接查询。

## 实验内容和要求

针对TPC-H数据库设计各种单表査询SQL语句、分组统计查询语句；设计单个表针对自身的连接査询，设计多个表的连接查询。理解和掌握SQL查询语句各个子句的特点和作 
用，按照SQL程序设计规范写出具体的SQL查询语句，并调试通过。
说明：简单地说，SQL程序设计规范包含SQL关键字大写、表名、属性名、存储过程名等 
标识符大小写混合、SQL程序书写缩进排列等编程规范。

## 实验重点和难点

实验重点：分组统计查询、单表自身连接查询、多表连接查询。
实验难点：区分元组过滤条件和分组过滤条件；确定连接属性，正确设计连接条件。

## 实验SQL语句

```sql
-- 单表查询（投影）--
-- 查询供应商名称、地址、联系电话 --
SELECT name, address ,phone FROM Supplier;

/*
单表查询
(实现选择操作)
査询最近一周内提交的总价大于1 000元的订单的编号、顾客编号等订单的所有 
信息。
*/
SELECT * FROM Sales.Orders
WHERE CURRENT_DATE - orderdate < 7 AND totalprice > 1000;

/*
不带分组过滤条件的分组统计査询 
统计每个顾客的订购金额。
*/
SELECT C.custkey , SUM( O.totalprice) /* 对每个组，作用聚集函数 SUM */
FROM Customer C , Orders O WHERE C.custkey = O.custkey
GROUP BY C.custkey; /* 按照 C.custkey 分组 */

/*
带分组过滤条件的分组统计查询
查询订单平均金额超过1000元的顾客编号及其姓名。
*/
SELECT C.custkey, MAX(C.name) /*分组属性和聚集函数才能出现在SELECT子句*/
FROM Customer C , Orders O WHERE C.custkey = O.custkey GROUP BY C.custkey /* 按照 C.custkey 分组 */ 
HAVING AVG(O.totalprice) > 1000;/* 按照条件对组进行过滤，只输出满足条件的组*/

/*
单表自身连接查询
查询与“金仓集团”在同一个国家的供应商编号、名称和地址信息。
*/
SELECT F.suppkey, F.name, F.address
FROM Supplier F, Supplier S/*Supplier 表的自身连接 */ 
WHERE F.nationkey = S.nationkey AND S.name = '金仓集团';

/*
两表连接查询(普通连接)
查询供应价格大于零售价格的零件名、制造商名、零售价格和供应价格。
*/
SELECT P.name, P.mfgr , P.retailprice , PS.supplycost
FROM Part P, PartSupp PS/*两表连接，给表起个别名，简化表达*/ 
WHERE P.retailprice > PS.supplycost;/* 限定条件 */
/* 说明：上述连接语句是从两个表的笛卡儿积中选出满足限定条件的元组，得到的结果可能 
不是同一个商品的有关值，所以应改为下面的自然连接*/

/*
两表连接查询(自然连接)
查询供应价格大于零售价格的零件名、制造商名、零售价格和供应价格。
*/
SELECT P.name, P.mfgr , P.retailprice , PS.supplycost
FROM Part P, PartSupp PS/* 两表连接，给表起个别名，简化表达*/
WHERE P.partkey = PS.partkey /* 连接条件 */
AND P.retailprice > PS.supplycost; /* 限定条件 */

/*
三表连接査询
查询顾客“苏举库”订购的订单编号、总价及其订购的零件编号、数量和明细价格。
*/
SELECT O.orderkey, O.totalprice, L.partkey, L.quantity , L.extendprice
FROM Customer C, Orders O, Lineitem L
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey AND C.name ='苏举库';
```

## 实验过程及结果

上述SQL语句的查询结果，过长的结果节选了部分：

```sql
-- 单表查询（投影）--
-- 查询供应商名称、地址、联系电话 --
SELECT name, address ,phone FROM Supplier;
```



![2018-10-10 21-21-46 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 21-21-46 的屏幕截图.png)

```mysql
/*
单表查询
(实现选择操作)
査询最近一周内提交的总价大于1 000元的订单的编号、顾客编号等订单的所有 
信息。
*/
SELECT * FROM Sales.Orders
WHERE CURRENT_DATE - orderdate < 7 AND totalprice > 1000;

```

![2018-10-10 21-22-29 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 21-22-29 的屏幕截图.png)

```mysql
/*
不带分组过滤条件的分组统计査询 
统计每个顾客的订购金额。
*/
SELECT C.custkey , SUM( O.totalprice) /* 对每个组，作用聚集函数 SUM */
FROM Customer C , Orders O WHERE C.custkey = O.custkey
GROUP BY C.custkey; /* 按照 C.custkey 分组 */

```



![2018-10-10 21-22-59 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 21-22-59 的屏幕截图.png)

```mysql
/*
带分组过滤条件的分组统计查询
查询订单平均金额超过1000元的顾客编号及其姓名。
*/
SELECT C.custkey, MAX(C.name) /*分组属性和聚集函数才能出现在SELECT子句*/
FROM Customer C , Orders O WHERE C.custkey = O.custkey GROUP BY C.custkey /* 按照 C.custkey 分组 */ 
HAVING AVG(O.totalprice) > 1000;/* 按照条件对组进行过滤，只输出满足条件的组*/
```



![2018-10-10 21-23-16 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 21-23-16 的屏幕截图.png)

```mysql
/*
单表自身连接查询
查询与“金仓集团”在同一个国家的供应商编号、名称和地址信息。
*/
SELECT F.suppkey, F.name, F.address
FROM Supplier F, Supplier S/*Supplier 表的自身连接 */ 
WHERE F.nationkey = S.nationkey AND S.name = '金仓集团';
```



![2018-10-10 21-23-53 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 21-23-53 的屏幕截图.png)

```mysql
/*
两表连接查询(普通连接)
查询供应价格大于零售价格的零件名、制造商名、零售价格和供应价格。
*/
SELECT P.name, P.mfgr , P.retailprice , PS.supplycost
FROM Part P, PartSupp PS/*两表连接，给表起个别名，简化表达*/ 
WHERE P.retailprice > PS.supplycost;/* 限定条件 */
/* 说明：上述连接语句是从两个表的笛卡儿积中选出满足限定条件的元组，得到的结果可能 
不是同一个商品的有关值，所以应改为下面的自然连接*/
```



![2018-10-10 21-24-13 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 21-24-13 的屏幕截图.png)

```mysql
/*
两表连接查询(自然连接)
查询供应价格大于零售价格的零件名、制造商名、零售价格和供应价格。
*/
SELECT P.name, P.mfgr , P.retailprice , PS.supplycost
FROM Part P, PartSupp PS/* 两表连接，给表起个别名，简化表达*/
WHERE P.partkey = PS.partkey /* 连接条件 */
AND P.retailprice > PS.supplycost; /* 限定条件 */

```



![2018-10-10 21-24-28 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 21-24-28 的屏幕截图.png)

```mysql
/*
三表连接査询
查询顾客“苏举库”订购的订单编号、总价及其订购的零件编号、数量和明细价格。
*/
SELECT O.orderkey, O.totalprice, L.partkey, L.quantity , L.extendprice
FROM Customer C, Orders O, Lineitem L
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey AND C.name ='苏举库';
```



![2018-10-10 21-24-39 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 21-24-39 的屏幕截图.png)



## 实验总结

1. 正确理解数据库模式结构，才能正确设计数据库查询；
2. 连接查询是数据库SQL查询中最重要的查询，连接查询的设计要特别注意，不同的查询表达，其查询执行的性能会有很大差别。

## 思考题

1. 不在GROUP BY子句出现的属性，是否可以出现在SELECT子句中？请举例并上 
   机验证。

   答：select中只能出现group by中的属性或是聚合函数，如下所示：

   ```sql
   -- Wrong --
   SELECT A, B, C, D
   FROM My_Table
   GROUP BY A, B, C;
   
   -- Correct --
   SELECT A, B, C, sum(D)
   FROM My_Table
   GROUP BY A, B, C;
   ```

2. 请举例说明分组统计查询中的WHERE和 HAVING有何区别？

   答：where语句必须对应表中已有的属性，而having语句则无此限制。典型表现就是having语句可以接受聚合函数作为条件，如下：

   ```sql
   SELECT *
   FROM My_Table
   WHERE user_age > 18;
   
   SELECT *
   FROM My_Table
   GROUP BY gender
   WHERE avg(user_age) > 18;
   ```

3. 连接查询速度是影响关系数据库性能的关键因素。请讨论如何提髙连接査询速度，并进行实验验证。

   答：尽量按需选择连接属性，避免盲目地进行多表全连接。