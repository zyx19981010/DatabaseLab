# 实验 1.4 数据更新实验

## 实验目的

熟悉数据库的数据更新操作，能够使用SQL语句对数据库进行数据的插人、修改、删除 
操作。

## 实验内容和要求

针对TPC-H数据库设计单元组插入、批量数据插入、修改数据和删除数据等SQL语句。 
理解和掌握INSERT、UPDATE和 DELETE语法结构的各个组成成分，结合嵌套SQL子査询， 
分别设计几种不同形式的插入、修改和删除数据的语句，并调试成功。

## 实验重点和难点

实验重点：插入、修改和删除数据的SQL。
实验难点：与嵌套SQL子查询相结合的插人、修改和删除数据的SQL语句；利用一个表 
的数据来插人、修改和删除另外一个表的数据。

## 实验SQL语句

```sql
/*
INSERT基本语句(插入全部列的数据)
插入一条顾客记录，要求每列都给一个合理的值。
*/
INSERT INTO Customer
VALUES(30, '张三', '北京市', 40, '010-51001199', 0.00, 'Northeast', 'VIP Customer');

/*
INSERT基本语句(插入部分列的数据)
插入一条订单记录，给出必要的几个字段值
*/
INSERT INTO Lineitem(orderkey, Linenumber, partkey, suppkey, quantity, shipdate)
VALUES(862, ROUND(RANDOM() * 100, 0), 479, 1, 10, '2012-3-6');

/*
批量数据INSERT语句
*/
-- 1.创建一个新的顾客表，把所有中国籍顾客插人到新的顾客表中。--
CREATE TABLE NewCustomer AS SELECT * FROM Customer WITH NO DATA;
INSERT INTO NewCustomer
SELECT C.*
FROM Customer C, Nation N
WHERE C.nationkey = N.nationkey AND N.name = '中国';

-- 2.创建一个顾客购物统计表，记录每个顾客及其购物总数和总价等信息。 --
CREATE TABLE ShoppingStat
(
    custkey INTEGER,
    quantity REAL,
    totalprice REAL
)
INSERT INTO ShoppingStat
SELECT C.custkey, Sum(L.quantity), Sum(O.totalprice)
FROM Customer C, Orders O, Lineitem L
GROUP BY C.custkey;

-- 3.倍增零件表的数据，多次重复执行，直到总记录数达到50万为止。--
INSERT INTO Part
SELECT partkey + (SELECT COUNT(*) FROM Part),
    name, mfgr, brand, type, size, container, retailprice, commet
FROM Part;

/*
UPDATE语句(修改部分记录的部分列值)
"金仓集团”供应的所有零件的供应成本价下降10%。
*/
UPDATE PartSupp
SET supplycost = supplycost * 0.9
WHERE suppkey = (SELECT suppkey
FROM Supplier
WHERE name = '金仓集团');

/*
UPDATE语句(利用一个表中的数据修改另外一个表中的数据)
利用Part表中的零售价格来修改Lineitem中的extendedprice，其中extendedprice = Part.retailprice * quantity。
*/
UPDATE Lineitem L
SET L.extendprice = P.retailprice * L.quantity
FROM Part P
WHERE L.partkey = P.partkey;

/*
DELETE基本语句(删除给定条件的所有记录)
删除顾客张三的所有订单记录。
*/
DELETE FROM Lineitem
WHERE orderkey IN (SELECT orderkey
FROM Orders O, Customer C
WHERE O.custkey = C.custkey AND C.name = '张三');
DELETE FROM Orders
WHERE custkey = (
    SELECT custkey
    FROM Customer
    WHERE name = '张三'
);
```

## 实验过程与结果

```mysql
/*
INSERT基本语句(插入全部列的数据)
插入一条顾客记录，要求每列都给一个合理的值。
*/
INSERT INTO Customer
VALUES(30, '张三', '北京市', 40, '010-51001199', 0.00, 'Northeast', 'VIP Customer');
```

![1539233219916](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539233219916.png)



```mysql
/*
INSERT基本语句(插入部分列的数据)
插入一条订单记录，给出必要的几个字段值
*/
INSERT INTO Lineitem(orderkey, Linenumber, partkey, suppkey, quantity, shipdate)
VALUES(862, ROUND(RAND() * 100, 0), 479, 1, 10, '2012-3-6');
```

![1539233422706](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539233422706.png)



```mysql
/*
批量数据INSERT语句
*/
-- 1.创建一个新的顾客表，把所有中国籍顾客插人到新的顾客表中。--
CREATE TABLE NewCustomer like Customer;
INSERT INTO NewCustomer
SELECT C.*
FROM Customer C, Nation N
WHERE C.nationkey = N.nationkey AND N.name = '中国';
```

![1539234604766](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539234604766.png)

```mysql
-- 2.创建一个顾客购物统计表，记录每个顾客及其购物总数和总价等信息。 --
CREATE TABLE ShoppingStat
(
    custkey INTEGER,
    quantity REAL,
    totalprice REAL
)
INSERT INTO ShoppingStat
SELECT C.custkey, Sum(L.quantity), Sum(O.totalprice)
FROM Customer C, Orders O, Lineitem L
GROUP BY C.custkey;
```

![1539234938032](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539234938032.png)



```mysql
-- 3.倍增零件表的数据，多次重复执行，直到总记录数达到50万为止。--
INSERT INTO Part
SELECT partkey + (SELECT COUNT(*) FROM Part),
    name, mfgr, brand, type, size, container, retailprice, comment
FROM Part;
```

![1539235280376](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539235280376.png)



```mysql
/*
UPDATE语句(修改部分记录的部分列值)
"金仓集团”供应的所有零件的供应成本价下降10%。
*/
UPDATE PartSupp
SET supplycost = supplycost * 0.9
WHERE suppkey = (SELECT suppkey
FROM Supplier
WHERE name = '金仓集团');
```

![1539235525955](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539235525955.png)



```mysql
/*
UPDATE语句(利用一个表中的数据修改另外一个表中的数据)
利用Part表中的零售价格来修改Lineitem中的extendedprice，其中extendedprice = Part.retailprice * quantity。
*/
UPDATE Lineitem L, Part P
SET L.extendprice = P.retailprice * L.quantity
WHERE L.partkey = P.partkey;
```



```mariadb
/*
DELETE基本语句(删除给定条件的所有记录)
删除顾客张三的所有订单记录。
*/
DELETE FROM Lineitem
WHERE orderkey IN (SELECT orderkey
FROM Orders O, Customer C
WHERE O.custkey = C.custkey AND C.name = '张三');
DELETE FROM Orders
WHERE custkey = (
    SELECT custkey
    FROM Customer
    WHERE name = '张三'
);
```



## 实验总结

1. 正确地设计和执行数据更新语句，确保正确地录人数据和更新数据，才能保证查询 
   出来的数据正确。
2. 当更新数据失败时，一个主要原因是更新数据时违反了完整性约束。

## 思考题

1. 请分析数据库模式更新和数据更新SQL语句的异同。

   答：两者都是对已经存在的数据库实体进行的修改。数据库模式更新更改表的结构，使用ADD，ALTER以及DROP等语句；数据更新更新表中的数据内容，使用INSERT，UPDATE以及DELETE等语句。

2. 请分析数据库系统除了 INSERT、UPDATE和 DELETE等基本的数据更新语句之外， 
   还有哪些可以用来更新数据库基本表数据的SQL语句？

   答：还有MERGE语句，该语句在SQL Server 2008之后版本及Oracle数据库中可用。该语句的功能是合并INSERT与UPDATE语句的语法，使数据更新指令更加简洁。