# 实验1.3 数据高级查询实验

## 实验目的

掌握SQL嵌套查询和集合查询等各种高级查询的设计方法等。

## 实验内容和要求

针对TPC-H数据库，正确分析用户查询要求，设计各种嵌套查询和集合查询。

## 实验重点和难点

实验重点:嵌套查询
实验难点：相关子查询、多层EXIST嵌套查询

## 实验SQL语句

```sql
/*
IN嵌套查询
査询订购了“海大”制造的“船舶模拟驾驶舱”的顾客。
*/
SELECT custkey, name
FROM Customer
WHERE custkey IN (SELECT O.custkey
FROM Orders O, Lineitem L, PartSupp PS, Part P /* 四表连接*/
WHERE O.orderkey = L.orderkey AND L.partkey = PS.partkey AND L.suppkey = PS.suppkey AND PS.partkey = P.partkey AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');

/*Lineitem表直接与Part表连接*/
SELECT custkey, name
FROM Customer
WHERE custkey IN
(SELECT O.custkey
FROM Orders O, Lineitem L, Part P
WHERE O.orderkey = L.orderkey AND
L.partkey = P.partkey
AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');

/*
单层EXISTS嵌套查询
査询没有购买过“海大”制造的“船舶模拟驾驶舱”的顾客。
*/
SELECT custkey, name
FROM Customer
WHERE NOT EXISTS (SELECT O.custkey
FROM Orders O, Lineitem L, PartSupp PS, Part P
/* 四表连接*/
WHERE O.orderkey = L.orderkey AND L.partkey = PS.partkey AND L.suppkey = PS.suppkey AND PS.partkey = P.partkey AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');

/*
双层EXISTS嵌套查询
查询至少购买过顾客“张三”购买过的全部零件的顾客姓名。
*/
SELECT CA.name /*查找CA客户，其不存在张三购买过而CA客户没有买过的零件*/ 
FROM Customer CA 
WHERE NOT EXISTS
(SELECT *
/*张三购买过而CA客户没有买过的零件*/
FROM Customer CB , Orders OB, Lineitem LB WHERE CB.custkey = OB.custkey AND OB.orderkey = LB.orderkey AND CB.name = '张三' AND NOT EXISTS
(SELECT * /* CA客户与CB客户都购买过的零件*/
FROM Orders OC , Lineitem LC 
WHERE CA.custkey = OC.custkey AND 
OC.orderkey = LC.orderkey AND
LB.suppkey = LC.suppkey AND LB.partkey = LC.partkey ) ) ;

/*
FROM子句中的嵌套査询
查询订单平均金额超过1万元的顾客中的中国籍顾客信息。
*/
SELECT C. *
FROM Customer C, ( SELECT custkey /* 子査询生成的临时派生表为B表*/
    FROM Orders
    GROUP BY custkey
    HAVING AVG( totalprice) > 10000) B, Nation N
WHERE C.custkey = B.custkey AND /*B 表成为主査询的査询对象*/ 
C.nationkey = N.nationkey AND N.name = '中国';

/*
集合査询(交)
查询顾客“张三”和"李四”都订购过的全部零件的信息。
*/
SELECT P.*
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey
AND L.suppkey = PS.suppkey
AND L.partkey = PS.partkey
AND PS.partkey = P.partkey
AND C.name = '张三';
INTERSECTION
SELECT P. *
FROM Customer C,Orders O, Lineitem L, PartSupp PS, Fart P 
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
AND L.suppkey = PS.suppkey AND L.partkey = PS.partkey AND
PS.partkey = P.partkey
AND C.name = '李四';

/*
集合查询(并)
查询顾客“张三”和“李四”订购的全部零件的信息。
*/
SELECT P. *
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
AND L.suppkey = PS.suppkey 
AND L.partkey = PS.partkey AND
PS.partkey= P.partkey
AND C.name = '张三';
UNION
SELECT P. *
FROM Customer C,Orders O, Lineitem L, PartSupp PS, Fart P 
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
AND L.suppkey = PS.suppkey AND L.partkey = PS.partkey AND
PS.partkey = P.partkey
AND C.name = '李四';

/*
集合查询(差)
顾客“张三”订购过而“李四”没订购过的零件的信息。
*/
SELECT P.*
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey
    AND O.orderkey = L.orderkey
    AND L.suppkey = PS.suppkey
    AND L.partkey = PS.partkey
    AND PS.partkey = P.partkey
    AND C.name = '张三'
EXCEPT
SELECT P.*
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey
    AND O.orderkey = L.orderkey
    AND L.suppkey = PS.suppkey
    AND L.partkey = PS.partkey
    AND PS.partkey = P.partkey
    AND C.name = '李四'
    
```

## 实验过程及结果

```mysql
/*
IN嵌套查询
査询订购了“海大”制造的“船舶模拟驾驶舱”的顾客。
*/
SELECT custkey, name
FROM Customer
WHERE custkey IN (SELECT O.custkey
FROM Orders O, Lineitem L, PartSupp PS, Part P /* 四表连接*/
WHERE O.orderkey = L.orderkey AND L.partkey = PS.partkey AND L.suppkey = PS.suppkey AND PS.partkey = P.partkey AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');

```

![1539186710270](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539186710270.png)



```mysql
/*Lineitem表直接与Part表连接*/
SELECT custkey, name
FROM Customer
WHERE custkey IN
(SELECT O.custkey
FROM Orders O, Lineitem L, Part P
WHERE O.orderkey = L.orderkey AND
L.partkey = P.partkey
AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');
```

![1539186747000](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539186747000.png)



```mysql
/*
单层EXISTS嵌套查询
査询没有购买过“海大”制造的“船舶模拟驾驶舱”的顾客。
*/
SELECT custkey, name
FROM Customer
WHERE NOT EXISTS (SELECT O.custkey
FROM Orders O, Lineitem L, PartSupp PS, Part P
/* 四表连接*/
WHERE O.orderkey = L.orderkey AND L.partkey = PS.partkey AND L.suppkey = PS.suppkey AND PS.partkey = P.partkey AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');
```

![1539186783145](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539186783145.png)



```mysql
/*
双层EXISTS嵌套查询
查询至少购买过顾客“张三”购买过的全部零件的顾客姓名。
*/
SELECT CA.name /*查找CA客户，其不存在张三购买过而CA客户没有买过的零件*/ 
FROM Customer CA 
WHERE NOT EXISTS
(SELECT *
/*张三购买过而CA客户没有买过的零件*/
FROM Customer CB , Orders OB, Lineitem LB WHERE CB.custkey = OB.custkey AND OB.orderkey = LB.orderkey AND CB.name = '张三' AND NOT EXISTS
(SELECT * /* CA客户与CB客户都购买过的零件*/
FROM Orders OC , Lineitem LC 
WHERE CA.custkey = OC.custkey AND 
OC.orderkey = LC.orderkey AND
LB.suppkey = LC.suppkey AND LB.partkey = LC.partkey ) ) ;
```

![1539186819128](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539186819128.png)



```mysql
/*
FROM子句中的嵌套査询
查询订单平均金额超过1万元的顾客中的中国籍顾客信息。
*/
SELECT C. *
FROM Customer C, ( SELECT custkey /* 子査询生成的临时派生表为B表*/
    FROM Orders
    GROUP BY custkey
    HAVING AVG( totalprice) > 10000) B, Nation N
WHERE C.custkey = B.custkey AND /*B 表成为主査询的査询对象*/ 
C.nationkey = N.nationkey AND N.name = '中国';
```

![1539186843922](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539186843922.png)



```mysql
/*
集合査询(交)
查询顾客“张三”和"李四”都订购过的全部零件的信息。
*/
-- 由于MySQL不支持INTERSECT关键字，改写为IN查询 --
SELECT P.*
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey
AND L.suppkey = PS.suppkey
AND L.partkey = PS.partkey
AND PS.partkey = P.partkey
AND C.name = '张三'
AND P.partkey
IN
    (SELECT P.partkey
    FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
    WHERE C.custkey = O.custkey
    AND O.orderkey = L.orderkey
    AND PS.partkey = P.partkey
    AND C.name = '李四');
```

![1539219594497](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539219594497.png)



```mysql
/*
集合查询(并)
查询顾客“张三”和“李四”订购的全部零件的信息。
*/
SELECT P. *
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
AND L.suppkey = PS.suppkey 
AND L.partkey = PS.partkey AND
PS.partkey= P.partkey
AND C.name = '张三'
UNION
SELECT P. *
FROM Customer C,Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
AND L.suppkey = PS.suppkey AND L.partkey = PS.partkey AND
PS.partkey = P.partkey
AND C.name = '李四';
```

![1539219681324](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539219681324.png)



```mysql
/*
集合查询(差)
顾客“张三”订购过而“李四”没订购过的零件的信息。
*/
-- 由于MySQL不支持EXCEPT关键字，改写为NOT IN --
SELECT P.*
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey
    AND O.orderkey = L.orderkey
    AND L.suppkey = PS.suppkey
    AND L.partkey = PS.partkey
    AND PS.partkey = P.partkey
    AND C.name = '张三'
    AND P.partkey
NOT IN
    (SELECT P.partkey
    FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
    WHERE C.custkey = O.custkey
    AND O.orderkey = L.orderkey
    AND L.suppkey = PS.suppkey
    AND L.partkey = PS.partkey
    AND PS.partkey = P.partkey
    AND C.name = '李四');
```

![1539219811972](/home/williamzheng/DatabaseLab/Reports/Exp_3.assets/1539219811972.png)

## 实验总结

通过分析图2 中 TPC-H数据库模式可知,Lineitem表 是 通 过 Partsupp表跟Part表联系的，所以，“ (1) 1N嵌套查询”中第一个查询是正常的查询表达方式。而由于partkey是 Part的主码，第二个查询也能得出相同的结果。因此，生成 Lineitem记录时利用PartSupp 表保证供应商和零件的一致性，而査询Lineitem时可以直接和Part相连接。同样，也可以直接和Suppliers相连接。

## 思考题

1. 试分析什么类型的査询可以用连接查询实现，什么类型的查询用只能用嵌套查询 
   实现？

   答：当两个表中的属性是一一结合时可以使用连接查询进行实现，如果是一对多则不适宜使用连接查询，否则可能导致多个相同结果。

2. 试分析不相关子查询和相关子查询的区别。

   答：不相关子查询的内层查询和外层查询没有先后依赖，可以分解为先执行内层查询再执行外层查询两步。而相关子查询则需要在内层查询中用到来自外层查询的属性。