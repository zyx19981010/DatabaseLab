# 实验1.1 数据库定义实验

## 实验目的

理解和掌握数据库DDL语言，能够熟练地使用SQL DDL语句创建、修改和删除数据库、模式和基本表。

## 实验内容和要求

理解和掌握SQL DDL语句的语法，特别是各种参数的具体含义和使用方法；使用SQL语句创建、修改和删除数据库、模式和基本表。掌握SQL语句常见语法错误的调试方法

## 实验重点和难点

实验重点：创建数据库、基本表。

实验难点：创建基本表时，为不同的列选择合适的数据类型，正确创建表级和列级完整性约束，如列值是否允许为空、主码和外码等。

## 实验SQL语句

```sql
create database Sales;
use Sales;
CREATE TABLE Region(
    regionkey INTEGER PRIMARY KEY,
    name CHAR(25),
    comment VARCHAR(152)
);

CREATE TABLE Nation(
    nationkey INTEGER PRIMARY KEY,
    name CHAR(25),
    regionkey INTEGER REFERENCES Region(regionkey),
    comment VARCHAR(152)
);

CREATE TABLE Supplier(
    suppkey INTEGER PRIMARY KEY,
    name CHAR(25),
    address VARCHAR(40),
    nationkey INTEGER REFERENCES Nation(nationkey),
    phone CHAR(15),
    acctbal REAL,
    comment VARCHAR(152)
);

CREATE TABLE Part(
    partkey INTEGER PRIMARY KEY,
    name VARCHAR(55),
    mfgr CHAR(25),
    brand CHAR(10),
    type VARCHAR(25),
    size INTEGER,
    container CHAR(10),
    retailprice REAL,
    comment VARCHAR(23)
);

CREATE TABLE PartSupp(
    partkey INTEGER REFERENCES Part(partkey),
    suppkey INTEGER REFERENCES Supplier(suppkey),
    availqty INTEGER,
    supplycost REAL,
    comment VARCHAR(199),
    PRIMARY KEY (partkey, suppkey)
);

CREATE TABLE Customer(
    custkey INTEGER PRIMARY KEY,
    name VARCHAR(25),
    address VARCHAR(40),
    nationkey INTEGER REFERENCES Nation(nationkey),
    phone CHAR(15),
    acctbal REAL,
    mktsegment CHAR(10),
    comment VARCHAR(117)
);

CREATE TABLE Orders(
    orderkey INTEGER PRIMARY KEY,
    custkey INTEGER REFERENCES Customer(custkey),
    orderstatus CHAR(1),
    totalprice REAL,
    orderdate DATE,
    orderpriority CHAR(15),
    clerk CHAR(15),
    shippriority INTEGER,
    comment VARCHAR(79)
);

CREATE TABLE Lineitem(
    orderkey INTEGER REFERENCES Orders(orderkey),
    partkey INTEGER REFERENCES Part(partkey),
    suppkey INTEGER REFERENCES Uupplier(suppkey),
    linenumber INTEGER,
    quantity REAL,
    extendprice REAL,
    discount REAL,
    tax REAL,
    returnflag CHAR(1),
    linestatus CHAR(1),
    shipdate DATE,
    commitdate DATE,
    receiptdate DATE,
    shipinstruct CHAR(25),
    shipmode CHAR(10),
    comment VARCHAR(44),
    PRIMARY KEY(orderkey, linenumber),
    FOREIGN KEY(partkey, suppkey) REFERENCES PartSupp(partkey, suppkey)
);
```

## 实验结果

执行查询并手动导入数据后的效果如下：

数据表：

![2018-10-10 19-37-57 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 19-37-57 的屏幕截图.png)

数据表结构：

![2018-10-10 19-58-25 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 19-58-25 的屏幕截图.png)

![2018-10-10 20-29-12 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 20-29-12 的屏幕截图.png)

![2018-10-10 20-29-51 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 20-29-51 的屏幕截图.png)



![2018-10-10 20-30-19 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 20-30-19 的屏幕截图.png)

![2018-10-10 20-30-41 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 20-30-41 的屏幕截图.png)

![2018-10-10 20-31-05 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 20-31-05 的屏幕截图.png)

![2018-10-10 20-31-28 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 20-31-28 的屏幕截图.png)

![2018-10-10 20-31-49 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 20-31-49 的屏幕截图.png)

数据表中的数据（以Lineitem表为例）：

![2018-10-10 19-34-59 的屏幕截图](/home/williamzheng/Pictures/2018-10-10 19-34-59 的屏幕截图.png)

## 实验总结

本次实验进行了数据库的定义和数据导入。实验过程中也遇到了一些问题，主要有以下几点：

- 课本上的数据库查询指令无法直接运行。由于课本上的数据查询指令是针对国产的金仓数据库运行的，而本机上的实验环境是遵循MySQL规则的MariaDB，因此需要对应的对指令做一些修正，使其符合MySQL的语法规则。
- 在导入数据时，为了安全起见，MariaDB默认不允许本地数据导入，需要在连接数据库时加上--local-infile选项。

总体来说第一个实验还是比较简单的，也没有遇到过多的困难，但是能够验证理论学到的知识，尤其是外键引用等各种稍微有些抽象的概念，对于理解知识也有着非常大的帮助。

## 思考题

1. SQL语法规定，双引号括定的符号串为对象名称，单引号括定的符号串为常量字符 
   串，那么什么情况下需要用双引号来界定对象名呢？请实验验证。

   答：当对象命称中包含空格等符号时，需要使用双引号(或是中括号)来界定对象名，如下列查询：

   ```sql
   SELECT * FROM "My Special Table"
   WHERE "Special User Name" = 'My User Name';
   ```

2. 数据库对象的完整引用是"服务器名.数据库名.模式名.对象名”，但通常可以省略 
   服务器名和数据库名，甚至模式名，直接用对象名访问对象即可。请设计相应的实验验证基本表及其列的访问方法。

   答：假设已经与目标数据库服务器建立连接，可以用以下语句查询指定数据库中指定表的列：

   ```sql
   using My_Database;
   SELECT * FROM my_table;
   ```

