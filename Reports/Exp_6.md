# 实验1.6 索引实验

## 实验目的

掌握索引设计原则和技巧，能够创建合适的索引以提高数据库查询、统计分析效率。

## 实验内容和要求

针对给定的数据库模式和具体应用需求，创建唯一索引、函数索引、复合索引等;修改索 
引；删除索引:设计相应的SQL查询验证索引有效性，学习利用EXPLAIN命令分析SQL査
询是否使用了所创建的索引，并能够分析其原因，执行SQL查询并估算索引提高查询效率的 
百分比。要求实验数据集达到10万条记录以上的数据量，以便验证索引效果。

## 实验重点和难点

实验重点：创建索引。
实验难点：设计SQL查询验证索引有效性。

## 实验SQL语句

```sql
/*
创建唯一索引
在零件表的零件名称字段上创建唯一索引。
*/
CREATE UNIQUE INDEX Idx_part_name ON Part(name);

/*
创建函数索引(对某个属性的函数创建索引，称为函数索引)
在零件表的零件名称字段上创建一个零件名称长度的函数索引。
*/
CREATE INDEX Idx_part_name_fun ON Part(LENGTH(name));

/*
创建复合索引(对两个及两个以上的属性创建索引，称为复合索引)
在零件表的制造商和品牌两个字段上创建一个复合索引。
*/
CREATE UNIQUE INDEX Idx_part_mfgr_brand ON Part(mfgr, brand);

/*
创建聚簇索引
在零件表的制造商字段上创建一个聚簇索引。
*/
CREATE UNIQUE INDEX Idx_part_mfgr ON Part(mfgr);
CLUSTER Idx_part_mfgr;

/*
创建Hash索引
在零件表的名称字段上创建一个Hash索引。
*/
CREATE INDEX Idx_part_name_hash ON Part USING HASH(name);

/*
修改索引名称
修改零件表的名称字段上的索引名。
*/
ALTER INDEX Idx_part_name_hash RENAME TO Idx_part_name_hash_new;

/*
分析某个SQL查询语句执行时是否使用了索引
*/
EXPLAIN SELECT * FROM part WHERE name = '零件';

/*
验证索引效率
创建一个函数Testlndex,自动计算SQL查询执行的时间。
*/
CREATE FUNCTION TestIndex(p_partname CHAR(55)) RETURN INTEGER
AS
DECLARE
    begintime TIMESTAMP;
    endtime TIMESTAMP;
    durationtime INTEGER;
BEGIN
    SELECT CLOCK_TIMESTAMP() INTO begintime;
    PERFORM * FROM Part WHERE name = p_partname;
    SELECT CLOCK_TIMESTAMP() INTO endtime;
    SELECT DATADIFF('ms', begintime, endtime) INTO durationtime;
    RETURN durartiontime;
END;
SELECT TestIndex('零件名称');
INSERT INTO Part
SELECT partkey + (SELECT COUNT(*) FROM Part),
    name, mfgr, brand, type, size, container, retailprice, comment
FROM Part;

SELECT TestIndex('零件名称');
CREATE INDEX part_name ON Part(name);
SELECT TestIndex();

```

## 实验过程及结果

```mariadb
/*
创建唯一索引
在零件表的零件名称字段上创建唯一索引。
*/
CREATE UNIQUE INDEX Idx_part_name ON Part(name);
-- 如果对应属性上有相同的值，查询会失败 --
```

![1539262507083](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539262507083.png)

清除上个实验中产生的多余值后：

![1539263507598](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539263507598.png)



```mariadb
/*
创建函数索引(对某个属性的函数创建索引，称为函数索引)
在零件表的零件名称字段上创建一个零件名称长度的函数索引。
*/
CREATE INDEX Idx_part_name_fun ON Part(LENGTH(name));
```



```mariadb
/*
创建复合索引(对两个及两个以上的属性创建索引，称为复合索引)
在零件表的制造商和品牌两个字段上创建一个复合索引。
*/
CREATE UNIQUE INDEX Idx_part_mfgr_brand ON Part(mfgr, brand);
```

![1539263550865](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539263550865.png)



```mariadb
/*
创建聚簇索引
在零件表的制造商字段上创建一个聚簇索引。
*/
-- 在MySQL中，主键所在的索引自动形成聚簇索引 --
ALTER TABLE  Part DROP PRIMARY KEY ,ADD PRIMARY KEY (partkey, mfgr);
CREATE UNIQUE INDEX Idx_part_mfgr ON Part(mfgr);
```

![1539264024582](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539264024582.png)



```mariadb
/*
创建Hash索引
在零件表的名称字段上创建一个Hash索引。
*/
-- 大部分MySQL存储引擎不支持Hash索引 --
CREATE INDEX Idx_part_name_hash ON Part USING HASH(name);
```

![1539267096064](/home/williamzheng/DatabaseLab/Reports/Exp_4.assets/1539267096064.png)







## 实验总结



## 思考题

1. 在一个表的多个字段上创建的复合索引，与在相应的每个字段上创建的多个简单索引有何异同？请设计相应的例子加以验证。

   答：在针对其中的第一个单个字段进行查询时，复合索引和简单索引的效率是类似的。但需要注意的是表中存在过多的简单索引会影响效率。

   在针对符合索引中的其它单个字段查询时，不会用到复合索引（在MySQL上成立）。

   在进行多字段的多条件查询时，如果条件中包含索引的第一个字段，则会用到复合索引。而只要查询中包含多个单个索引的任何一个字段，都会用到相应的单个索引。





