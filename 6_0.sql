USE Sales;
# (1)无参数的存储过程
# 定义一个存储过程，更新所有订单的(含税折扣价)总价
CREATE PROCEDURE Proc_Cal_TotalPrice()
BEGIN
  UPDATE Orders SET totalprice =
                      (SELECT SUM(extendprice * (1- discount) * (1 + tax))
                        FROM Lineitem
                        WHERE Orders.orderkey = Lineitem.orderkey);
end;
# 执行存储过程Proc_Cal_TotalPrice()
CALL Proc_Cal_TotalPrice();

# (2)有参数的存储过程
# 定义一个存储过程，更新给定订单的(含税折扣价)总价
CREATE PROCEDURE Proc_Cal_TotalPrice4Order(okey INTEGER)
BEGIN
  UPDATE Orders SET totalprice =
                      (SELECT SUM(extendprice * (1- discount) * (1 + tax))
                        FROM Lineitem
                        WHERE Orders.orderkey = Lineitem.orderkey
                        AND Lineitem.orderkey = okey);
end;
# 执行存储过程Proc_Cal_TotalPrice4Order()
CALL Proc_Cal_TotalPrice4Order(5365);

# (3)有局部变量的存储过程
# 定义一个存储过程，更新某个顾客的所有订单的(含税折扣价)总价
CREATE PROCEDURE Proc_Cal_TotalPrice4Customer(p_custname CHAR(25))
BEGIN
  DECLARE L_custkey INTEGER;
  SELECT custkey INTO L_custkey
    FROM Customer
    where name = TRIM(p_custname);
  UPDATE Orders SET totalprice =
                      (SELECT SUM(extendprice * (1- discount) * (1 + tax))
                        FROM Lineitem
                        WHERE Orders.orderkey = Lineitem.orderkey
                        AND Orders.custkey = L_custkey);
end;
# 执行存储过程Proc_Cal_TotalPrice4Customer()
CALL Proc_Cal_TotalPrice4Customer('陈楷丰');

# 查看存储过程执行结果
SELECT * FROM Orders
WHERE custkey = (SELECT custkey FROM Customer WHERE name = '陈楷丰');

# (4)有输出参数的存储过程
# 定义一个存储过程，更新某个顾客的所有订单的(含税折扣价)总价
CREATE PROCEDURE Proc_Cal_TotalPrice4Customer2(p_custname CHAR(25), OUT p_totalprice REAL)
BEGIN
  DECLARE L_custkey INTEGER;
  SELECT custkey INTO L_custkey
    FROM Customer
    WHERE name= trim(p_custname);
  # RAISE NOTICE 'custkey is %', L_custkey;
  UPDATE Orders SET totalprice =
                      (SELECT SUM(extendprice * (1- discount) * (1 + tax))
                        FROM Lineitem
                        WHERE Orders.orderkey = Lineitem.orderkey
                        AND Orders.custkey = L_custkey);
  SELECT SUM(totalprice) INTO p_totalprice
  FROM Orders WHERE custkey = L_custkey;
end;

# 执行存储过程Proc_Cal_TotalPrice4Customer2()
# MySQL不接受NULL参数
SET @num = 9;
CALL Proc_Cal_TotalPrice4Customer2('陈楷丰', @num);

# 查看存储过程执行结果
SELECT SUM(totalprice)
FROM Orders
WHERE custkey = (SELECT custkey
  FROM Customer
  WHERE name = '陈楷丰');

# (5)修改存储过程
# 修改存储过程名Proc_Cal_TotalPrice4Order为CalTotalPrice4Order
# ALTER PROCEDURE Proc_Cal_TotalPrice4Order RENAME TO CalTotalPrice4Order;

# 编译存储过程
# ALTER PROCEDURE Proc_Cal_TotalPrice4Order COMPILE;
# 删除存储过程
DROP PROCEDURE Proc_Cal_TotalPrice4Order;