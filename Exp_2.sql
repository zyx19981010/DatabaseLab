-- 单表查询（投影）--
-- 查询供应商名称、地址、联系电话 --
SELECT name, address ,phone FROM Supplier;

/*
单表查询
(实现选择操作)
査询最近一周内提交的总价大于1 000元的订单的编号、顾客编号等订单的所有 
信息。
*/
SELECT * FROM Sales.Orders
WHERE CURRENT_DATE - orderdate < 7 AND totalprice > 1000;

/*
不带分组过滤条件的分组统计査询 
统计每个顾客的订购金额。
*/
SELECT C.custkey , SUM( O.totalprice) /* 对每个组，作用聚集函数 SUM */
FROM Customer C , Orders O WHERE C.custkey = O.custkey
GROUP BY C.custkey; /* 按照 C.custkey 分组 */

/*
带分组过滤条件的分组统计查询
查询订单平均金额超过1000元的顾客编号及其姓名。
*/
SELECT C.custkey, MAX(C.name) /*分组属性和聚集函数才能出现在SELECT子句*/
FROM Customer C , Orders O WHERE C.custkey = O.custkey GROUP BY C.custkey /* 按照 C.custkey 分组 */ 
HAVING AVG(O.totalprice) > 1000;/* 按照条件对组进行过滤，只输出满足条件的组*/

/*
单表自身连接查询
查询与“金仓集团”在同一个国家的供应商编号、名称和地址信息。
*/
SELECT F.suppkey, F.name, F.address
FROM Supplier F, Supplier S/*Supplier 表的自身连接 */ 
WHERE F.nationkey = S.nationkey AND S.name = '金仓集团';

/*
两表连接查询(普通连接)
查询供应价格大于零售价格的零件名、制造商名、零售价格和供应价格。
*/
SELECT P.name, P.mfgr , P.retailprice , PS.supplycost
FROM Part P, PartSupp PS/*两表连接，给表起个别名，简化表达*/ 
WHERE P.retailprice > PS.supplycost;/* 限定条件 */
/* 说明：上述连接语句是从两个表的笛卡儿积中选出满足限定条件的元组，得到的结果可能 
不是同一个商品的有关值，所以应改为下面的自然连接*/

/*
两表连接查询(自然连接)
查询供应价格大于零售价格的零件名、制造商名、零售价格和供应价格。
*/
SELECT P.name, P.mfgr , P.retailprice , PS.supplycost
FROM Part P, PartSupp PS/* 两表连接，给表起个别名，简化表达*/
WHERE P.partkey = PS.partkey /* 连接条件 */
AND P.retailprice > PS.supplycost; /* 限定条件 */

/*
三表连接査询
查询顾客“苏举库”订购的订单编号、总价及其订购的零件编号、数量和明细价格。
*/
SELECT O.orderkey, O.totalprice, L.partkey, L.quantity , L.extendprice
FROM Customer C, Orders O, Lineitem L
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey AND C.name ='苏举库';