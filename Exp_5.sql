/*
创建视图(省略视图列名)
创建一个"海大汽配”供应商供应的零件视图V_DLMU_PartSuppl ,要求列出供应零件 的编号、零件名称、可用数量、零售价格、供应价格和备注等信息。
*/
CREATE VIEW V_DLMU_PartSupp1 AS
SELECT P.partkey, P.name, PS.availqty, P.retailprice, PS.supplycost, P.comment
FROM Part P, PartSupp PS, Supplier S
WHERE P.partkey = PS.partkey
AND S.suppkey = PS.suppkey
AND S.name = '海大汽配';

/*
创建视图(不能省略列名的情况)
创建一个视图V_CustAvgOrder，按顾客统计平均每个订单的购买金额和零件数量，要求输出顾客编号，姓名，平均购买金额和平均购买零件数量。
*/
CREATE VIEW V_CustAvgOrder(custkey, cname, avgprice, avgquantity) AS
SELECT C.custkey, MAX(C.name), AVG(O.totalprice), AVG(L.quantity)
FROM Customer C, Orders O, Lineitem L
WHERE C.custkey = O.custkey AND L.orderkey = O.orderkey
GROUP BY C.custkey;

/*
创建视图(WITH CHECK OPTION)
使用WITH CHECK OPTION,创建一个“海大汽配”供应商供应的零件视图V_DLMU_ PartSupp2,
要求列出供应零件的编号、可用数量和供应价格等信息。
然后通过该视图分别增加、刪除和修改一条"海大汽配”零件供应记录，验证 WITH CHECK OPTION是否起作用。
*/
CREATE VIEW V_DLMU_PartSupp2
AS
SELECT partkey, suppkey, availqty, supplycost
FROM PartSupp
WHERE suppkey = (
    SELECT suppkey
    FROM Supplier
    WHERE name = '海大汽配'
)
WITH CHECK OPTION;

INSERT INTO V_DLMU_PartSupp2
VALUES(58889, 5048, 704, 77760);

UPDATE V_DLMU_PartSupp2
SET supplycost = 12
WHERE supplycost = 58889;

DELETE FROM V_DLMU_PartSupp2
WHERE suppkey = 58889;

/*
可更新的视图
(行列子集视图)
创建一个“海大汽配”供应商供应的零件视图V_DLMU_PartSupp4,要求列出供应零件的编号、可用数量和供应价格等信息。
然后通过该视图分别增加、删除和修改一条“海大汽配”零件供应记录，
验证该视图是否是可更新的，并比较上述"(3) 创建视图"实验任务 
与本任务结果有何异同。
*/
CREATE VIEW V_DLMU_PartSupp3
AS
SELECT partkey, suppkey, availqty, supplycost
FROM PartSupp
WHERE suppkey = (
    SELECT suppkey
    FROM Supplier
    WHERE name = '海大汽配'
);

INSERT INTO V_DLMU_PartSupp3
VALUES(58889, 5048, 704, 77760);

UPDATE V_DLMU_PartSupp3
SET supplycost = 12
WHERE supplycost = 58889;

DELETE FROM V_DLMU_PartSupp3
WHERE suppkey = 58889;

/*
不可更新的视图
以下列语句验证（2）中创建的视图是否可更新
*/
INSERT INTO V_CustAvgOrder
VALUES(100000, NULL, 20, 2000);

/*
删除视图(RESTRICT/CASCADE)
创建顾客订购零件明细视图V_CUStOrd，要求列出顾客编号、姓名、购买零件数、金额， 
然后在该视图的基础上，再创建(2)的视图V_CustAvgOrder，然后使用RESTRICT选项删除
视图V_CustOrd，观察现象并解释原因。利用CASCADE选项删除视图V_CustOrd，观察现 
象并检查V_CustAVgOrder是否存在，解释原因？
*/
CREATE VIEW V_CustOrd(custkey, cname, qty, extprice)
AS
SELECT C.custkey, C.name, L.quantity, L.extendprice
from Customer C, Orders O, Lineitem L
WHERE C.custkey = O.custkey
AND O.orderkey = L.orderkey;

CREATE VIEW V_CustAvgOrder(custkey, cname, avgqty, avgprice)
AS
SELECT custkey, MAX(cname), AVG(qty), AVG(extprice)
FROM V_CustOrd
GROUP BY custkey;

DROP VIEW V_CustOrd RESTRICT;
DROP VIEW V_CustOrd CASCADE;

