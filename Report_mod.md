# 实验1 数据库定义与操作语言实验

## 实验1.1 数据库定义实验

1. 实验目的

   理解和掌握数据库DDL语言，能够熟练地使用SQL DDL语句创建、修改和删除数据库、模式和基本表。

2. 实验内容和要求

   理解和掌握SQL DDL语句的语法，特别是各种参数的具体含义和使用方法；使用SQL语句创建、修改和删除数据库、模式和基本表。掌握SQL语句常见语法错误的调试方法

3. 实验重点和难点

   实验重点：创建数据库、基本表。

   实验难点：创建基本表时，为不同的列选择合适的数据类型，正确创建表级和列级完整性约束，如列值是否允许为空、主码和外码等。

4. 实验过程

   ```sql
   CREATE TABLE Region(
       regionkey INTEGER PRIMARY KEY,
       name CHAR(25),
       comment VARCHAR(152)
   )
   
   CREATE TABLE Nation(
       nationkey INTEGER PRIMARY KEY,
       name CHAR(25),
       regionkey INTEGER REFERENCES Region(regionkey),
       comment VARCHAR(152)
   )
   
   CREATE TABLE Supplier(
       suppkey INTEGER PRIMARY KEY,
       name CHAR(25),
       address VARCHAR(40),
       nationkey INTEGER REFERENCES Nation(nationkey),
       phone CHAR(15),
       acctbal REAL,
       comment VARCHAR(152)
   )
   
   CREATE TABLE Part(
       partkey INTEGER PRIMARY KEY,
       name VARCHAR(55),
       mfgr CHAR(25),
       brand CHAR(10),
       type VARCHAR(25),
       size INTEGER,
       container CHAR(10),
       retailprice REAL,
       comment VARCHAR(23)
   )
   
   CREATE TABLE PartSupp(
       partkey INTEGER REFERENCES Part(partkey),
       suppkey INTEGER REFERENCES Supplier(suppkey),
       availqty INTEGER,
       supplycost REAL,
       comment VARCHAR(199),
       PRIMARY KEY (partkey, suppkey)
   )
   
   CREATE TABLE Customer(
       custkey INTEGER PRIMARY KEY,
       name VARCHAR(25),
       address VARCHAR(40),
       nationkey INTEGER REFERENCES Nation(nationkey),
       phone CHAR(15),
       acctbal REAL,
       mktsegment CHAR(10),
       comment VARCHAR(117)
   )
   
   CREATE TABLE Orders(
       orderkey INTEGER PRIMARY KEY,
       custkey INTEGER REFERENCES Customer(custkey),
       orderstatus CHAR(1),
       totalprice REAL,
       orderdate DATE,
       orderpriority CHAR(15),
       clerk CHAR(15),
       shippriority INTEGER,
       comment VARCHAR(79)
   )
   
   CREATE TABLE Lineitem(
       orderkey INTEGER REFERENCES Orders(orderkey),
       partkey INTEGER REFERENCES Part(partkey),
       suppkey INTEGER REFERENCES Uupplier(suppkey),
       linenumber INTEGER,
       quantity REAL,
       extendprice REAL,
       discount REAL,
       tax REAL,
       returnflag CHAR(1),
       linestatus CHAR(1),
       shipdate DATE,
       commitdate DATE,
       receiptdate DATE,
       shipinstruct CHAR(25),
       shipmode CHAR(10),
       comment VARCHAR(44),
       PRIMARY KEY(orderkey, linenumber),
       FOREIGN KEY(partkey, suppkey) REFERENCES PartSupp(partkey, suppkey)
   );
   ```

5. 思考题

   1. SQL语法规定，双引号括定的符号串为对象名称，单引号括定的符号串为常量字符 
      串，那么什么情况下需要用双引号来界定对象名呢？请实验验证。

      答：当对象命称中包含空格等符号时，需要使用双引号(或是中括号)来界定对象名，如下列查询：

      ```sql
      SELECT * FROM "My Special Table"
      WHERE "Special User Name" = 'My User Name';
      ```

   2. 数据库对象的完整引用是"服务器名.数据库名.模式名.对象名”，但通常可以省略 
      服务器名和数据库名，甚至模式名，直接用对象名访问对象即可。请设计相应的实验验证基本表及其列的访问方法。

      答：假设已经与目标数据库服务器建立连接，可以用以下语句查询指定数据库中指定表的列：

      ```sql
      using My_Database;
      SELECT * FROM my_table;
      ```

## 实验 1.2 数据基本查询实验

1. 实验目的

   掌握SQL程序设计基本规范，熟练运用SQL语言实现数据基本査询，包括单表査询、分 
   组统计査询和连接查询。

2. 实验内容和要求

   针对TPC-H数据库设计各种单表査询SQL语句、分组统计查询语句；设计单个表针对自身的连接査询，设计多个表的连接查询。理解和掌握SQL查询语句各个子句的特点和作 
   用，按照SQL程序设计规范写出具体的SQL查询语句，并调试通过。
   说明：简单地说，SQL程序设计规范包含SQL关键字大写、表名、属性名、存储过程名等 
   标识符大小写混合、SQL程序书写缩进排列等编程规范。

3. 实验重点和难点

   实验重点：分组统计查询、单表自身连接查询、多表连接查询。
   实验难点：区分元组过滤条件和分组过滤条件；确定连接属性，正确设计连接条件。

4. 实验过程

   ```sql
   -- 单表查询（投影）--
   -- 查询供应商名称、地址、联系电话 --
   SELECT name, address ,phone FROM Supplier;
   
   /*
   单表查询
   (实现选择操作)
   査询最近一周内提交的总价大于1 000元的订单的编号、顾客编号等订单的所有 
   信息。
   */
   SELECT * FROM Sales.Orders
   WHERE CURRENT_DATE - orderdate < 7 AND totalprice > 1000;
   
   /*
   不带分组过滤条件的分组统计査询 
   统计每个顾客的订购金额。
   */
   SELECT C.custkey , SUM( O.lotalprice) /* 对每个组，作用聚集函数 SUM */
   FROM Customer C , Orders O WHERE C.custkey = O.custkey
   GROUP BY C.custkey; /* 按照 C.custkey 分组 */
   
   /*
   带分组过滤条件的分组统计查询
   查询订单平均金额超过1000元的顾客编号及其姓名。
   */
   SELECT C.custkey, MAX(C.name) /*分组属性和聚集函数才能出现在SELECT子句*/
   FROM Customer C , Orders O WHERE C.custkey = O.custkey GROUP BY C.custkey /* 按照 C.custkey 分组 */ 
   HAVING AVG(O.totalprice) > 1000;/* 按照条件对组进行过滤，只输出满足条件的组*/
   
   /*
   单表自身连接查询
   查询与“金仓集团”在同一个国家的供应商编号、名称和地址信息。
   */
   SELECT F.suppkey, F.na me, F.address
   FROM Supplier F, Supplier S/*Supplier 表的自身连接 */ 
   WHERE F.nationkey = S.nationkey AND S.name = '金仓集团';
   
   /*
   两表连接查询(普通连接)
   查询供应价格大于零售价格的零件名、制造商名、零售价格和供应价格。
   */
   SELECT P.name, P.m fgr , P.retailo price , FS.supplycost
   FROM Part P, PartSupp PS/*两表连接，给表起个别名，简化表达*/ 
   WHERE P.retailprice > PS.supplycost;/* 限定条件 */
   /* 说明：上述连接语句是从两个表的笛卡儿积中选出满足限定条件的元组，得到的结果可能 
   不是同一个商品的有关值，所以应改为下面的自然连接*/
   
   /*
   两表连接查询(自然连接)
   查询供应价格大于零售价格的零件名、制造商名、零售价格和供应价格。
   */
   SELECT P.nam e, P.m fgr , P.retailo price , PS.supplycost
   FROM Part P, PartSupp PS/* 两表连接，给表起个别名，简化表达*/
   WHERE P.partkey = PS.partkey /* 连接条件 */
   AND P.retailprice > PS.supplycost; /* 限定条件 */
   
   /*
   三表连接査询
   查询顾客“苏举库”订购的订单编号、总价及其订购的零件编号、数量和明细价格。
   */
   SELECT O.orderkey, O.totalprice, L.partkey, L.quantity , L.extendedprice 
   FROM Customer C, Orders O, Lineitem L
   WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey AND C.name ='苏举库';
   ```

   实验总结

   1. 正确理解数据库模式结构，才能正确设计数据库查询；
   2. 连接查询是数据库SQL查询中最重要的查询，连接查询的设计要特别注意，不同的 
      查询表达，其查询执行的性能会有很大差别。

5. 思考题

   1. 不在GROUP BY子句出现的属性，是否可以出现在SELECT子句中？请举例并上 
      机验证。

      答：select中只能出现group by中的属性或是聚合函数，如下所示：

      ```sql
      -- Wrong --
      SELECT A, B, C, D
      FROM My_Table
      GROUP BY A, B, C;
      
      -- Correct --
      SELECT A, B, C, sum(D)
      FROM My_Table
      GROUP BY A, B, C;
      ```

   2. 请举例说明分组统计查询中的WHERE和 HAVING有何区别？

      答：where语句必须对应表中已有的属性，而having语句则无此限制。典型表现就是having语句可以接受聚合函数作为条件，如下：

      ```sql
      SELECT *
      FROM My_Table
      WHERE user_age > 18;
      
      SELECT *
      FROM My_Table
      GROUP BY gender
      WHERE avg(user_age) > 18;
      ```

   3. 连接查询速度是影响关系数据库性能的关键因素。请讨论如何提髙连接査询速度，并进行实验验证。

      答：尽量按需选择连接属性，避免盲目地进行多表全连接。

## 实验1.3 数据高级查询实验

1. 实验目的

   掌握SQL嵌套查询和集合查询等各种高级查询的设计方法等。

2. 实验内容和要求

   针对TPC-H数据库，正确分析用户查询要求，设计各种嵌套查询和集合查询。

3. 实验重点和难点

   实验重点:嵌套查询
   实验难点：相关子查询、多层EXIST嵌套查询

4. 实验过程

   ```sql
   /*
   IN嵌套查询
   査询订购了“海大”制造的“船舶模拟驾驶舱”的顾客。
   */
   SELECT custkey, name
   FROM Customer
   WHERE custkey IN (SELECT O.custkey
   FROM Orders O, Lineitem L, PartSupp PS, Part P /* 四表连接*/
   WHERE O.orderkey = L.orderkey AND L.partkey = PS.partkey AND L.suppkey = PS.suppkey AND PS.partkey = P.partkey AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');
   
   /*Lineitem表直接与Part表连接*/
   SELECT custkey, name
   FROM Customer
   WHERE custkey IN
   (SELECT O.custkey
   FROM Orders O, Lineitem L, Part P
   WHERE O.orderkey = L.orderkey AND
   L.partkey = P.partkey
   AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');
   
   /*
   单层EXISTS嵌套查询
   査询没有购买过“海大”制造的“船舶模拟驾驶舱”的顾客。
   */
   SELECT custkey, name
   FROM Customer
   WHERE NOT EXISTS (SELECT O.custkey
   FROM Orders O, Lineitem L, PartSupp PS, Part P
   /* 四表连接*/
   WHERE O.orderkey = L.orderkey AND L.partkey = PS.partkey AND L.suppkey = PS.suppkey AND PS.partkey = P.partkey AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');
   
   /*
   双层EXISTS嵌套查询
   查询至少购买过顾客“张三”购买过的全部零件的顾客姓名。
   */
   SELECT CA.name /*查找CA客户，其不存在张三购买过而CA客户没有买过的零件*/ 
   FROM Customer CA 
   WHERE NOT EXISTS
   (SELECT *
   /*张三购买过而CA客户没有买过的零件*/
   FROM Customer CB , Orders OB, Lineitem LB WHERE CB.custkey = OB.custkey AND OB.orderkey = LB.orderkey AND CB.name = '张三' AND NOT EXISTS
   (SELECT * /* CA客户与CB客户都购买过的零件*/
   FROM Orders OC , Lineitem LC 
   WHERE CA.custkey = OC.custkey AND 
   OC.orderkey = LC.orderkey AND
   LB.suppkey = LC.suppkey AND LB.partkey = LC.partkey ) ) ;
   
   /*
   FROM子句中的嵌套査询
   查询订单平均金额超过1万元的顾客中的中国籍顾客信息。
   */
   SELECT C. *
   FROM Customer C, ( SELECT custkey /* 子査询生成的临时派生表为B表*/
       FROM Orders
       GROUP BY custkey
       HAVING AVG( totalprice) > 10000) B, Nation N
   WHERE C.custkey = B.custkey AND /*B 表成为主査询的査询对象*/ 
   C.nationkey = N.nationkey AND N.name = '中国';
   
   /*
   集合査询(交)
   查询顾客“张三”和"李四”都订购过的全部零件的信息。
   */
   SELECT P.*
   FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
   WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey
   AND L.suppkey = PS.suppkey
   AND L.partkey = PS.partkey
   AND PS.partkey = P.partkey
   AND C.name = '张三';
   INTERSECTION
   SELECT P.*
   FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
   WHERE C.custkey = O.custkey
   AND O.orderkey = L.orderkey
   
   /*
   集合查询(并)
   查询顾客“张三”和“李四”订购的全部零件的信息。
   */
   SELECT P. *
   FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
   WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
   AND L.suppkey = PS.suppkey 
   AND L.partkey = PS.partkey AND
   PS.partkey= P.partkey
   AND C.name = '张三';
   UNION
   SELECT P. *
   FROM Customer C,Orders O, Lineitem L, PartSupp PS, Fart P 
   WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
   AND L.suppkey = PS.suppkey AND L.partkey = PS.partkey AND
   PS.partkey = P.partkey
   AND C.name = '李四';
   
   /*
   集合查询(差)
   顾客“张三”订购过而“李四”没订购过的零件的信息。
   */
   SELECT P.*
   FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
   WHERE C.custkey = O.custkey
       AND O.orderkey = L.orderkey
       AND L.suppkey = PS.suppkey
       AND L.partkey = PS.partkey
       AND PS.partkey = P.partkey
       AND C.name = '张三'
   EXCEPT
   SELECT P.*
   FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
   WHERE C.custkey = O.custkey
       AND O.orderkey = L.orderkey
       AND L.suppkey = PS.suppkey
       AND L.partkey = PS.partkey
       AND PS.partkey = P.partkey
       AND C.name = '李四'
       
   ```

   实验总结：
   通过分析图2 中 TPC-H数据库模式可知,Lineitem表 是 通 过 Partsupp表跟Part表联系的，所以，“ (1) 1N嵌套查询”中第一个查询是正常的查询表达方式。而由于partkey是 Part的主码，第二个查询也能得出相同的结果。因此，生成 Lineitem记录时利用PartSupp 表保证供应商和零件的一致性，而査询Lineitem时可以直接和Part相连接。同样，也可以直接和Suppliers相连接。

5. 思考题

   1. 试分析什么类型的査询可以用连接查询实现，什么类型的查询用只能用嵌套查询 
      实现？

      答：当两个表中的属性是一一结合时可以使用连接查询进行实现，如果是一对多则不适宜使用连接查询，否则可能导致多个相同结果。

   2. 试分析不相关子查询和相关子查询的区别。

      答：不相关子查询的内层查询和外层查询没有先后依赖，可以分解为先执行内层查询再执行外层查询两步。而相关子查询则需要在内层查询中用到来自外层查询的属性。

## 实验 1.4

1. 实验目的

   熟悉数据库的数据更新操作，能够使用SQL语句对数据库进行数据的插人、修改、删除 
   操作。

2. 实验内容和要求

   针对TPC-H数据库设计单元组插入、批量数据插入、修改数据和删除数据等SQL语句。 
   理解和掌握INSERT、UPDATE和 DELETE语法结构的各个组成成分，结合嵌套SQL子査询， 
   分别设计几种不同形式的插入、修改和删除数据的语句，并调试成功。

3. 实验重点和难点

   实验重点：插入、修改和删除数据的SQL。
   实验难点：与嵌套SQL子查询相结合的插人、修改和删除数据的SQL语句；利用一个表 
   的数据来插人、修改和删除另外一个表的数据。

4. 实验过程

   ```sql
   /*
   INSERT基本语句(插入全部列的数据)
   插入一条顾客记录，要求每列都给一个合理的值。
   */
   INSERT INTO Customer
   VALUES(30, '张三', '北京市', 40, '010-51001199', 0.00, 'Northeast', 'VIP Customer');
   
   /*
   INSERT基本语句(插入部分列的数据)
   插入一条订单记录，给出必要的几个字段值
   */
   INSERT INTO Lineitem(orderkey, Linenumber, partkey, suppkey, quantity, shipdate)
   VALUES(862, ROUND(RANDOM() * 100, 0), 479, 1, 10, '2012-3-6');
   
   /*
   批量数据INSERT语句
   */
   -- 1.创建一个新的顾客表，把所有中国籍顾客插人到新的顾客表中。--
   CREATE TABLE NewCustomer AS SELECT * FROM Customer WITH NO DATA;
   INSERT INTO NewCustomer
   SELECT C.*
   FROM Customer C, Nation N
   WHERE C.nationkey = N.nationkey AND N.name = '中国';
   
   -- 2.创建一个顾客购物统计表，记录每个顾客及其购物总数和总价等信息。 --
   CREATE TABLE ShoppingStat
   (
       custkey INTEGER,
       quantity REAL,
       totalprice REAL
   )
   INSERT INTO ShoppingStat
   SELECT C.custkey, Sum(L.quantity), Sum(O.totalprice)
   FROM Customer C, Orders O, Lineitem L
   GROUP BY C.custkey;
   
   -- 3.倍增零件表的数据，多次重复执行，直到总记录数达到50万为止。--
   INSERT INTO Part
   SELECT partkey + (SELECT COUNT(*) FROM Part),
       name, mfgr, brand, type, size, container, retailprice, commet
   FROM Part;
   
   /*
   UPDATE语句(修改部分记录的部分列值)
   "金仓集团”供应的所有零件的供应成本价下降10%。
   */
   UPDATE PartSupp
   SET supplycost = supplycost * 0.9
   WHERE suppkey = (SELECT suppkey
   FROM Supplier
   WHERE name = '金仓集团');
   
   /*
   UPDATE语句(利用一个表中的数据修改另外一个表中的数据)
   利用Part表中的零售价格来修改Lineitem中的extendedprice，其中extendedprice = Part.retailprice * quantity。
   */
   UPDATE Lineitem L
   SET L.extendprice = P.retailprice * L.quantity
   FROM Part P
   WHERE L.partkey = P.partkey;
   
   /*
   DELETE基本语句(删除给定条件的所有记录)
   删除顾客张三的所有订单记录。
   */
   DELETE FROM Lineitem
   WHERE orderkey IN (SELECT orderkey
   FROM Orders O, Customer C
   WHERE O.custkey = C.custkey AND C.name = '张三');
   DELETE FROM Orders
   WHERE custkey = (
       SELECT custkey
       FROM Customer
       WHERE name = '张三'
   );
   ```

   实验总结：

   1. 正确地设计和执行数据更新语句，确保正确地录人数据和更新数据，才能保证查询 
      出来的数据正确。
   2. 当更新数据失败时，一个主要原因是更新数据时违反了完整性约束。

5. 思考题

   1. 请分析数据库模式更新和数据更新SQL语句的异同。

      答：两者都是对已经存在的数据库实体进行的修改。数据库模式更新更改表的结构，使用ADD，ALTER以及DROP等语句；数据更新更新表中的数据内容，使用INSERT，UPDATE以及DELETE等语句。

   2. 请分析数据库系统除了 INSERT、UPDATE和 DELETE等基本的数据更新语句之外， 
      还有哪些可以用来更新数据库基本表数据的SQL语句？

      答：还有MERGE语句，该语句在SQL Server 2008之后版本及Oracle数据库中可用。该语句的功能是合并INSERT与UPDATE语句的语法，使数据更新指令更加简洁。

## 实验 1.5 视图实验

1. 实验目的

   熟悉SQL语言有关视图的操作，能够熟练使用SQL语句来创建需要的视图，定义数据库 
   外模式，并能使用所创建的视图实现数据管理。

2. 实验内容和要求

   针对给定的数据库模式，以及相应的应用需求，创建视图和带WITH CHECK OPTION的 
   视图，并验证视图WITH CHECK OPTION选项的有效性、理解和掌握视图消解执行原理，掌 
   握可更新视图和不可更新视图的区别。

3. 实验重点和难点

   实验重点：创建视图。
   实验难点：可更新的视图和不可更新的视图之区别，WITH CHECK OPTION的验证。

4. 实验过程

   ```sql
   /*
   创建视图(省略视图列名)
   创建一个"海大汽配”供应商供应的零件视图V_DLMU_PartSuppl ,要求列出供应零件 的编号、零件名称、可用数量、零售价格、供应价格和备注等信息。
   */
   CREATE VIEW V_DLMU_PartSupp1 AS
   SELECT P.partkey, P.name, PS.availqty, P.retailprice, PS.supplycost, P.comment
   FROM Part P, PartSupp PS, Supplier S
   WHERE P.partkey = PS.partkey
   AND S.suppkey = PS.suppkey
   AND S.name = '海大汽配';
   
   /*
   创建视图(不能省略列名的情况)
   创建一个视图V_CustAvgOrder，按顾客统计平均每个订单的购买金额和零件数量，要求输出顾客编号，姓名，平均购买金额和平均购买零件数量。
   */
   CREATE VIEW V_CustAvgOrder(custkey, cname, avgprice, avgquantity) AS
   SELECT C.custkey, MAX(C.name), AVG(O.totalprice), AVG(L.quantity)
   FROM Customer C, Orders O, Lineitem L
   WHERE C.custkey = O.custkey AND L.orderkey = O.orderkey
   GROUP BY C.custkey;
   
   /*
   创建视图(WITH CHECK OPTION)
   使用WITH CHECK OPTION,创建一个“海大汽配”供应商供应的零件视图V_DLMU_ PartSupp2,
   要求列出供应零件的编号、可用数量和供应价格等信息。
   然后通过该视图分别增加、刪除和修改一条"海大汽配”零件供应记录，验证 WITH CHECK OPTION是否起作用。
   */
   CREATE VIEW V_DLMU_PartSupp2
   AS
   SELECT partkey, suppkey, availqty, supplycost
   FROM PartSupp
   WHERE suppkey = (
       SELECT suppkey
       FROM Supplier
       WHERE name = '海大汽配'
   )
   WITH CHECK OPTION;
   -- 可以执行 --
   INSERT INTO V_DLMU_PartSupp2
   VALUES(58889, 5048, 704, 77760);
   -- 可以执行 --
   UPDATE V_DLMU_PartSupp2
   SET supplycost = 12
   WHERE supplycost = 58889;
   -- 可以执行 --
   DELETE FROM V_DLMU_PartSupp2
   WHERE suppkey = 58889;
   
   /*
   可更新的视图(行列子集视图)
   创建一个“海大汽配”供应商供应的零件视图V_DLMU_PartSupp4,要求列出供应零件的编号、可用数量和供应价格等信息。
   然后通过该视图分别增加、删除和修改一条“海大汽配”零件供应记录，
   验证该视图是否是可更新的，并比较上述"(3) 创建视图"实验任务 
   与本任务结果有何异同。
   */
   CREATE VIEW V_DLMU_PartSupp3
   AS
   SELECT partkey, suppkey, availqty, supplycost
   FROM PartSupp
   WHERE suppkey = (
       SELECT suppkey
       FROM Supplier
       WHERE name = '海大汽配'
   );
   -- 可以执行 --
   INSERT INTO V_DLMU_PartSupp2
   VALUES(58889, 5048, 704, 77760);
   -- 可以执行 --
   UPDATE V_DLMU_PartSupp2
   SET supplycost = 12
   WHERE supplycost = 58889;
   -- 可以执行 --
   DELETE FROM V_DLMU_PartSupp2
   WHERE suppkey = 58889;
   
   /*
   不可更新的视图
   以下列语句验证（2）中创建的视图是否可更新
   */
   
   -- 执行失败，不允许直接对含聚合函数的视图作修改操作 --
   INSERT INTO V_CustAvgOrder
   VALUES(100000, NULL, 20, 2000);
   
   /*
   删除视图(RESTRICT/CASCADE)
   创建顾客订购零件明细视图V_CUStOrd，要求列出顾客编号、姓名、购买零件数、金额， 
   然后在该视图的基础上，再创建(2)的视图V_CustAvgOrder，然后使用RESTRICT选项删除 
   视图V_CustOrd，观察现象并解释原因。利用CASCADE选项删除视图V_CustOrd，观察现 
   象并检查V_CustAvgOrder是否存在，解释原因？
   */
   CREATE VIEW V_CustOrd(custkey, cname, qty, extprice)
   AS
   SELECT C.custkey = O.custkey
   AND O.orderkey = L.orderkey;
   
   CREATE VIEW V_CustAvgOrder(custkey, cname, avgqty, avgprice)
   AS
   SELECT custkey, MAX(cname), AVG(qty), AVG(extprice)
   FROM V_CustOrd
   GROUP BY custkey;
   
   -- 执行失败，由于存在另一个视图与之相关 --
   DROP VIEW V_CustOrd RESTRICT;
   -- 执行成功且V_CustAvgOrder消失。由于CASCADE移除所有相关的视图。--
   DROP VIEW V_CustOrd CASCADE;
   ```

5. 思考题

   1. 请分析视图和基本表在使用方面有哪些异同，并设计相应的例子加以验证。

      答：视图中的数据来源于基本表，两者都可以在权限范围内进行基本的数据查询操作。但在实现上，实际的数据存放于基本表中，视图中只存在数据的定义。在修改数据时，可以对基本表中的数据进行任意的修改，而对视图中数据的修改是受限的。

   2. 请具体分析修改基本表的结构对相应的视图会产生何种影响？

      答：如果被修改基本表的结构变动不涉及到视图中的属性，那么对视图没有影响。如果有视图中的属性在基本表中被移除了，那么该属性也会从视图中被移除。

## 实验1.6 索引实验

1. 实验目的

   掌握索引设计原则和技巧，能够创建合适的索引以提高数据库查询、统计分析效率。

2. 实验内容和要求

   针对给定的数据库模式和具体应用需求，创建唯一索引、函数索引、复合索引等;修改索 
   引；删除索引:设计相应的SQL查询验证索引有效性，学习利用EXPLAIN命令分析SQL査
   询是否使用了所创建的索引，并能够分析其原因，执行SQL查询并估算索引提高查询效率的 
   百分比。要求实验数据集达到10万条记录以上的数据量，以便验证索引效果。

3. 实验重点和难点

   实验重点：创建索引。
   实验难点：设计SQL查询验证索引有效性。

4. 实验过程

   ```sql
   /*
   创建唯一索引
   在零件表的零件名称字段上创建唯一索引。
   */
   CREATE UNIQUE INDEX Idx_part_name ON Part(name);
   
   /*
   创建函数索引(对某个属性的函数创建索引，称为函数索引)
   在零件表的零件名称字段上创建一个零件名称长度的函数索引。
   */
   CREATE INDEX Idx_part_name_fun ON Part(LENGTH(name));
   
   /*
   创建复合索引(对两个及两个以上的属性创建索引，称为复合索引)
   在零件表的制造商和品牌两个字段上创建一个复合索引。
   */
   CREATE UNIQUE INDEX Idx_part_mfgr_brand ON Part(mfgr, brand);
   
   /*
   创建聚簇索引
   在零件表的制造商字段上创建一个聚簇索引。
   */
   CREATE UNIQUE INDEX Idx_part_mfgr ON Part(mfgr);
   CLUSTER Idx_part_mfgr;
   
   /*
   创建Hash索引
   在零件表的名称字段上创建一个Hash索引。
   */
   CREATE INDEX Idx_part_name_hash ON Part USING HASH(name);
   
   /*
   修改索引名称
   修改零件表的名称字段上的索引名。
   */
   ALTER INDEX Idx_part_name_hash RENAME TO Idx_part_name_hash_new;
   
   /*
   分析某个SQL查询语句执行时是否使用了索引
   */
   EXPLAIN SELECT * FROM part WHERE name = '零件';
   
   /*
   验证索引效率
   创建一个函数Testlndex,自动计算SQL查询执行的时间。
   */
   CREATE FUNCTION TestIndex(p_partname CHAR(55)) RETURN INTEGER
   AS
   DECLARE
       begintime TIMESTAMP;
       endtime TIMESTAMP;
       durationtime INTEGER;
   BEGIN
       SELECT CLOCK_TIMESTAMP() INTO begintime;
       PERFORM * FROM Part WHERE name = p_partname;
       SELECT CLOCK_TIMESTAMP() INTO endtime;
       SELECT DATADIFF('ms', begintime, endtime) INTO durationtime;
       RETURN durartiontime;
   END;
   SELECT TestIndex('零件名称');
   INSERT INTO Part
   SELECT partkey + (SELECT COUNT(*) FROM Part),
       name, mfgr, brand, type, size, container, retailprice, comment
   FROM Part;
   
   SELECT TestIndex('零件名称');
   CREATE INDEX part_name ON Part(name);
   SELECT TestIndex();
   
   ```

5. 思考题

   1. 在一个表的多个字段上创建的复合索引，与在相应的每个字段上创建的多个简单索引有 
      何异同？请设计相应的例子加以验证。

      答：在针对其中的第一个单个字段进行查询时，复合索引和简单索引的效率是类似的。但需要注意的是表中存在过多的简单索引会影响效率。

      在针对符合索引中的其它单个字段查询时，不会用到复合索引（在MySQL上成立）。

      在进行多字段的多条件查询时，如果条件中包含索引的第一个字段，则会用到复合索引。而只要查询中包含多个单个索引的任何一个字段，都会用到相应的单个索引。





