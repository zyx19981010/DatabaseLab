use Sales;
-- 定义属性NULL/NOT NULL约束 --
CREATE TABLE region(
  regionkey INTEGER NOT NULL PRIMARY KEY ,
  name CHAR(25) NOT NULL ,
  comment VARCHAR(152)NULL
);

-- 定义属性DEFAULT约束 --
CREATE TABLE nation_1(
  nationkey INTEGER PRIMARY KEY ,
  name CHAR(25),
  regionkey INTEGER DEFAULT 0,
  comment VARCHAR(152),
  CONSTRAINT FK_Nation_regionkey FOREIGN KEY (regionkey) REFERENCES Region(regionkey)
);

-- 定义属性UNIQUE约束 --
CREATE TABLE nation_2(
  nationkey INTEGER PRIMARY KEY ,
  name CHAR(25) UNIQUE ,
  regionkey INTEGER,
  comment VARCHAR(152)
);

-- 使用CHECK --
CREATE TABLE Lineitem_1(
  orderkey INTEGER REFERENCES Orders(orderkey),
  partkey INTEGER REFERENCES Part(partkey),
  suppkey INTEGER REFERENCES Supplier(suppkey),
  linenumber INTEGER,
  quantity REAL,
  extendedprice REAL,
  discount REAL,
  tax REAL,
  returnflag CHAR(1),
  linestatus CHAR(1),
  shipdate DATE,
  commitdate DATE,
  receiptdate DATE,
  shipinstruct CHAR(25),
  shipmode CHAR(10),
  comment VARCHAR(44),
  PRIMARY KEY (orderkey, linenumber),
  FOREIGN KEY (partkey, suppkey) REFERENCES PartSupp(partkey, suppkey),
  CONSTRAINT CHECK (shipdate < receiptdate),
  CONSTRAINT CHECK (returnflag IN ('A', 'R', 'N'))
);
-- MySQL不支持CHECK，使用触发器取代 --
CREATE TRIGGER Lineitem_1_insert_check
  BEFORE INSERT ON Lineitem_1
  FOR EACH ROW
  BEGIN
    IF NEW.shipdate > NEW.receiptdate THEN
      SIGNAL SQLSTATE 'HY000' SET MESSAGE_TEXT = 'Invalid shipdate and receiptdate';
    end if;
    IF NEW.returnflag NOT IN ('A', 'R', 'N') THEN
      SIGNAL SQLSTATE 'HY000' SET MESSAGE_TEXT = 'Invalid returnflag';
    end if;
  end;

CREATE TRIGGER Lineitem_1_update_check
  BEFORE UPDATE ON Lineitem_1
  FOR EACH ROW
  BEGIN
    IF NEW.shipdate > NEW.receiptdate THEN
      SIGNAL SQLSTATE 'HY000' SET MESSAGE_TEXT = 'Invalid shipdate and receiptdate';
    end if;
    IF NEW.returnflag NOT IN ('A', 'R', 'N') THEN
      SIGNAL SQLSTATE 'HY000' SET MESSAGE_TEXT = 'Invalid returnflag';
    end if;
  end;

-- 插入测试使用的记录 --
INSERT INTO Lineitem_1 (orderkey, partkey, suppkey, linenumber, quantity, extendedprice, discount, tax, returnflag, linestatus, shipdate, commitdate, receiptdate, shipinstruct, shipmode, comment)
VALUES (5005, 1, 2, 1, 1.0, 1.0, 0.0, 0.0, 'A', 'A', '2015-01-01', '2015-01-05', '2015-01-10', 'ABCDEFGHIJKLMNOPQRSTUVW', 'ABCDEFGHIJ', 'NO COMMENT');
-- 修改Lineitem的一条记录验证是否违反CHECK约束 --
UPDATE Lineitem_1 SET shipdate = '2015-01-05', receiptdate = '2015-01-01'
WHERE orderkey = 5005 AND linenumber = 1;


DROP TABLE region, nation_1, nation_2, Lineitem_1;