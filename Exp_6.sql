/*
创建唯一索引
在零件表的零件名称字段上创建唯一索引。
*/
CREATE UNIQUE INDEX Idx_part_name ON Part(name);

/*
创建函数索引(对某个属性的函数创建索引，称为函数索引)
在零件表的零件名称字段上创建一个零件名称长度的函数索引。
*/
CREATE INDEX Idx_part_name_fun ON Part(LENGTH(name));
/*
创建复合索引(对两个及两个以上的属性创建索引，称为复合索引)
在零件表的制造商和品牌两个字段上创建一个复合索引。
*/
CREATE UNIQUE INDEX Idx_part_mfgr_brand ON Part(mfgr, brand);

/*
创建聚簇索引
在零件表的制造商字段上创建一个聚簇索引。
*/
-- 在MySQL中，主键所在的索引自动形成聚簇索引 --
ALTER TABLE  Part DROP PRIMARY KEY ,ADD PRIMARY KEY (partkey, mfgr);
CREATE UNIQUE INDEX Idx_part_mfgr ON Part(mfgr);


/*
创建Hash索引
在零件表的名称字段上创建一个Hash索引。
*/
CREATE INDEX Idx_part_name_hash ON Part USING HASH(name);

/*
修改索引名称
修改零件表的名称字段上的索引名。
*/
ALTER INDEX Idx_part_name_hash RENAME TO Idx_part_name_hash_new;

/*
分析某个SQL查询语句执行时是否使用了索引
*/
EXPLAIN SELECT * FROM part WHERE name = '零件';

/*
验证索引效率
创建一个函数Testlndex,自动计算SQL查询执行的时间。
*/
CREATE FUNCTION TestIndex(p_partname CHAR(55)) RETURN INTEGER
AS
DECLARE
    begintime TIMESTAMP;
    endtime TIMESTAMP;
    durationtime INTEGER;
BEGIN
    SELECT CLOCK_TIMESTAMP() INTO begintime;
    PERFORM * FROM Part WHERE name = p_partname;
    SELECT CLOCK_TIMESTAMP() INTO endtime;
    SELECT DATADIFF('ms', begintime, endtime) INTO durationtime;
    RETURN durartiontime;
END;
SELECT TestIndex('零件名称');
INSERT INTO Part
SELECT partkey + (SELECT COUNT(*) FROM Part),
    name, mfgr, brand, type, size, container, retailprice, comment
FROM Part;

SELECT TestIndex('零件名称');
CREATE INDEX part_name ON Part(name);
SELECT TestIndex();
