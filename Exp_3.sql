/*
IN嵌套查询
査询订购了“海大”制造的“船舶模拟驾驶舱”的顾客。
*/
SELECT custkey, name
FROM Customer
WHERE custkey IN (SELECT O.custkey
FROM Orders O, Lineitem L, PartSupp PS, Part P /* 四表连接*/
WHERE O.orderkey = L.orderkey AND L.partkey = PS.partkey AND L.suppkey = PS.suppkey AND PS.partkey = P.partkey AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');

/*Lineitem表直接与Part表连接*/
SELECT custkey, name
FROM Customer
WHERE custkey IN
(SELECT O.custkey
FROM Orders O, Lineitem L, Part P
WHERE O.orderkey = L.orderkey AND
L.partkey = P.partkey
AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');

/*
单层EXISTS嵌套查询
査询没有购买过“海大”制造的“船舶模拟驾驶舱”的顾客。
*/
SELECT custkey, name
FROM Customer
WHERE NOT EXISTS (SELECT O.custkey
FROM Orders O, Lineitem L, PartSupp PS, Part P
/* 四表连接*/
WHERE O.orderkey = L.orderkey AND L.partkey = PS.partkey AND L.suppkey = PS.suppkey AND PS.partkey = P.partkey AND P.mfgr = '海大' AND P.name = '船舶模拟驾驶舱');

/*
双层EXISTS嵌套查询
查询至少购买过顾客“张三”购买过的全部零件的顾客姓名。
*/
SELECT CA.name /*查找CA客户，其不存在张三购买过而CA客户没有买过的零件*/ 
FROM Customer CA 
WHERE NOT EXISTS
(SELECT *
/*张三购买过而CA客户没有买过的零件*/
FROM Customer CB , Orders OB, Lineitem LB WHERE CB.custkey = OB.custkey AND OB.orderkey = LB.orderkey AND CB.name = '张三' AND NOT EXISTS
(SELECT * /* CA客户与CB客户都购买过的零件*/
FROM Orders OC , Lineitem LC 
WHERE CA.custkey = OC.custkey AND 
OC.orderkey = LC.orderkey AND
LB.suppkey = LC.suppkey AND LB.partkey = LC.partkey ) ) ;

/*
FROM子句中的嵌套査询
查询订单平均金额超过1万元的顾客中的中国籍顾客信息。
*/
SELECT C. *
FROM Customer C, ( SELECT custkey /* 子査询生成的临时派生表为B表*/
    FROM Orders
    GROUP BY custkey
    HAVING AVG( totalprice) > 10000) B, Nation N
WHERE C.custkey = B.custkey AND /*B 表成为主査询的査询对象*/ 
C.nationkey = N.nationkey AND N.name = '中国';

/*
集合査询(交)
查询顾客“张三”和"李四”都订购过的全部零件的信息。
*/
SELECT P.*
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey
AND L.suppkey = PS.suppkey
AND L.partkey = PS.partkey
AND PS.partkey = P.partkey
AND C.name = '张三'
AND P.partkey
IN
    (SELECT P.partkey
    FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
    WHERE C.custkey = O.custkey
    AND O.orderkey = L.orderkey
    AND PS.partkey = P.partkey
    AND C.name = '李四');

/*
集合查询(并)
查询顾客“张三”和“李四”订购的全部零件的信息。
*/
SELECT P. *
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
AND L.suppkey = PS.suppkey 
AND L.partkey = PS.partkey AND
PS.partkey= P.partkey
AND C.name = '张三'
UNION
SELECT P. *
FROM Customer C,Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey AND O.orderkey = L.orderkey 
AND L.suppkey = PS.suppkey AND L.partkey = PS.partkey AND
PS.partkey = P.partkey
AND C.name = '李四';

/*
集合查询(差)
顾客“张三”订购过而“李四”没订购过的零件的信息。
*/
SELECT P.*
FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
WHERE C.custkey = O.custkey
    AND O.orderkey = L.orderkey
    AND L.suppkey = PS.suppkey
    AND L.partkey = PS.partkey
    AND PS.partkey = P.partkey
    AND C.name = '张三'
    AND P.partkey
NOT IN
    (SELECT P.partkey
    FROM Customer C, Orders O, Lineitem L, PartSupp PS, Part P
    WHERE C.custkey = O.custkey
    AND O.orderkey = L.orderkey
    AND L.suppkey = PS.suppkey
    AND L.partkey = PS.partkey
    AND PS.partkey = P.partkey
    AND C.name = '李四');
    