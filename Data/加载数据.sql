LOAD DATA LOCAL INFILE '/home/williamzheng/DatabaseLab/Data/region.tbl' REPLACE INTO TABLE Sales.Region FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';
LOAD DATA LOCAL INFILE '/home/williamzheng/DatabaseLab/Data/nation.tbl' REPLACE INTO TABLE Sales.Nation FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';
LOAD DATA LOCAL INFILE '/home/williamzheng/DatabaseLab/Data/supplier.tbl' REPLACE INTO TABLE Sales.Supplier FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';
LOAD DATA LOCAL INFILE '/home/williamzheng/DatabaseLab/Data/part.tbl' REPLACE INTO TABLE Sales.Part FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';
LOAD DATA LOCAL INFILE '/home/williamzheng/DatabaseLab/Data/partsupp.tbl' REPLACE INTO TABLE Sales.PartSupp FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';
LOAD DATA LOCAL INFILE '/home/williamzheng/DatabaseLab/Data/customer.tbl' REPLACE INTO TABLE Sales.Customer FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';
LOAD DATA LOCAL INFILE '/home/williamzheng/DatabaseLab/Data/orders.tbl' REPLACE INTO TABLE Sales.Orders FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';
LOAD DATA LOCAL INFILE '/home/williamzheng/DatabaseLab/Data/lineitem.tbl' REPLACE INTO TABLE Sales.Lineitem FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n';

